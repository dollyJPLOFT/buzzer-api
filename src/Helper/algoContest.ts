/**
 * 
 * @param videosForContest 8 videos selected for contest
 * @returns rounds to be played in contest
 */
export const getRounds = async (videosForContest) => {
    const casesForRound1 = {
        1: [
            {1: videosForContest[6], 2: videosForContest[1]},
            {1: videosForContest[5], 2: videosForContest[2]},
            {1: videosForContest[0], 2: videosForContest[7]},
            {1: videosForContest[3], 2: videosForContest[4]}
        ],
        2: [
            {1: videosForContest[7], 2: videosForContest[0]},
            {1: videosForContest[1], 2: videosForContest[6]},
            {1: videosForContest[2], 2: videosForContest[5]},
            {1: videosForContest[4], 2: videosForContest[3]}
        ],
        3: [
            {1: videosForContest[2], 2: videosForContest[5]},
            {1: videosForContest[1], 2: videosForContest[6]},
            {1: videosForContest[4], 2: videosForContest[3]},
            {1: videosForContest[7], 2: videosForContest[0]},
        ],
        4: [
            {1: videosForContest[1], 2: videosForContest[6]},
            {1: videosForContest[2], 2: videosForContest[5]},
            {1: videosForContest[7], 2: videosForContest[0]},
            {1: videosForContest[4], 2: videosForContest[3]}
        ],
        5: [
            {1: videosForContest[5], 2: videosForContest[2]},
            {1: videosForContest[0], 2: videosForContest[7]},
            {1: videosForContest[3], 2: videosForContest[4]},
            {1: videosForContest[6], 2: videosForContest[1]},
        ],
        6: [
            {1: videosForContest[6], 2: videosForContest[1]},
            {1: videosForContest[3], 2: videosForContest[4]},
            {1: videosForContest[0], 2: videosForContest[7]},
            {1: videosForContest[5], 2: videosForContest[2]},
        ],
    }

    const casesForRound2 = {
        1: [
            {1: videosForContest[3], 2: videosForContest[1]},
            {1: videosForContest[0], 2: videosForContest[2]}
        ],
        2: [
            {1: videosForContest[3], 2: videosForContest[1]},
            {1: videosForContest[2], 2: videosForContest[0]}
        ],
        3: [
            {1: videosForContest[1], 2: videosForContest[3]},
            {1: videosForContest[0], 2: videosForContest[2]}
        ],
        4: [
            {1: videosForContest[1], 2: videosForContest[3]},
            {1: videosForContest[2], 2: videosForContest[0]}
        ],
        5: [
            {1: videosForContest[0], 2: videosForContest[2]},
            {1: videosForContest[3], 2: videosForContest[1]}
        ],
        6: [
            {1: videosForContest[2], 2: videosForContest[0]},
            {1: videosForContest[3], 2: videosForContest[1]}
        ],
        7: [
            {1: videosForContest[0], 2: videosForContest[2]},
            {1: videosForContest[1], 2: videosForContest[3]}
        ],
        8: [
            {1: videosForContest[2], 2: videosForContest[0]},
            {1: videosForContest[1], 2: videosForContest[3]}
        ]
    };

    const casesForRound3 = {
        1: [
            {1: videosForContest[1], 2: videosForContest[0]}
        ],
        2: [
            {1: videosForContest[0], 2: videosForContest[1]}
        ]
    };

    const randomNumber1 = randomNumber(1,6);
    const randomNumber2 = randomNumber(1,8);
    const randomNumber3 = randomNumber(1,2);

    console.log(casesForRound1, casesForRound2, casesForRound3);

    return {
        round1: casesForRound1[randomNumber1],
        round2: casesForRound2[randomNumber2],
        round3: casesForRound3[randomNumber3]
    }
}

/**
 * 
 * @param min number
 * @param max number
 * @returns random numner in between min and max range
 */
function randomNumber(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}