/**
 * 
 * @param query  query
 * @param limit  number of docs to fetch = 10
 * @param page  page number
 * @param model  model
 * @returns  array
 */
export const videosFlow = async (query, limit, page, model) => {
    let allContestVideo, contestVideos1 = [], contestVideos2 = [], contestVideos3 = [], contestVideos4 = [];
    // 15% of the limit(10) = 2(ceil)
    const percentCount1 = Math.ceil((15 / 100) * limit);
    // 5% of the limit(10) = 1(ceil)
    const percentCount2 = Math.ceil((5 / 100) * limit);
    // 30% of the limit(10) = 3(ceil)
    const percentCount3 = Math.ceil((30 / 100) * limit);
    // 50% of the limit(10) = 5(ceil)
    const percentCount4 = Math.ceil((50 / 100) * limit);
    console.log(percentCount1, percentCount2, percentCount3, percentCount4);
    // 2 videos having user viewed count less than 250
    if (percentCount1 > 0) {
        contestVideos1 =  await model.aggregate([
            { $match: query },
            {
                $sort: {
                    createdAt: -1
                }
            },
            {
                $project: {
                    _id: 1,
                    userLikedCount: {
                        $size: '$userLiked'
                    },
                    userViewedCount: {
                        $size: '$userViewed'
                    }
                }
            },
            {
                $match: {
                    'userViewedCount': {$lt: 250}
                }
            },
            {
                $skip: (page - 1) * percentCount1
            },
            {
                $limit: percentCount1
            },
            {
                $project: {
                    _id: 1
                }
            }
        ]);
        console.log("contestVideos1 : ", contestVideos1);
    }
    // 1 video having user liked count is less then 10% of user viewed count.
    if (percentCount2 > 0) {
        contestVideos2 =  await model.aggregate([
            { $match: query },
            {
                $sort: {
                    createdAt: -1
                }
            },
            {
                $project: {
                    _id: 1,
                    userLikedCount: {
                        $size: '$userLiked'
                    },
                    userViewedCount: {
                        $size: '$userViewed'
                    }
                }
            },
            {
                $match: {
                    'userViewedCount': {$gte: 1}
                }
            },
            {
                $project: {
                    percentage: {$multiply: [{$divide: [ '$userLikedCount', '$userViewedCount' ]}, 100]}
                }
            }, 
            {
                $match: {
                    percentage: {$lte: 10}
                }
            },
            {
                $skip: (page - 1) * percentCount2
            },
            {
                $limit: percentCount2
            },
            {
                $project: {
                    _id: 1
                }
            }
        ]);
        console.log("contestVideos2 : ", contestVideos2);
    }

    // 3 videos having user liked count is greater then 10% and less then 20% of user viewed count.
    if (percentCount3 > 0) {
        contestVideos3 =  await model.aggregate([
            { $match: query },
            {
                $sort: {
                    createdAt: -1
                }
            },
            {
                $project: {
                    _id: 1,
                    userLikedCount: {
                        $size: '$userLiked'
                    },
                    userViewedCount: {
                        $size: '$userViewed'
                    }
                }
            }, 
            {
                $match: {
                    $and: [
                        {'userViewedCount': {$gte: 1}},
                        {'userLikedCount': {$gte: 50}}
                    ] 
                }
            }, {
                $project: {
                    percentage: {$multiply: [{$divide: [ '$userLikedCount', '$userViewedCount' ]}, 100]}
                }
            }, {
                $match: {
                    percentage: {$gt: 10, $lte: 20}
                }
            },
            {
                $skip: (page - 1) * percentCount3
            },
            {
                $limit: percentCount3
            },
            {
                $project: {
                    _id: 1
                }
            }
        ]);
        console.log("contestVideos3 : ", contestVideos3);
    }

    // 5 videos having user liked count, greater than 50 and is greater than 20% of user viewed count.
    if (percentCount4 > 0) {
        contestVideos4 =  await model.aggregate([
            { $match: query },
            {
                $sort: {
                    createdAt: -1
                }
            },
            {
                $project: {
                    _id: 1,
                    userLikedCount: {
                        $size: '$userLiked'
                    },
                    userViewedCount: {
                        $size: '$userViewed'
                    }
                }
            }, {
                $match: {
                    $and: [
                        {'userViewedCount': {$gte: 1}},
                        {'userLikedCount': {$gte: 50}}
                    ] 
                }
            }, {
                $project: {
                    percentage: {$multiply: [{$divide: [ '$userLikedCount', '$userViewedCount' ]}, 100]}
                }
            }, {
                $match: {
                    percentage: {$gte: 20}
                }
            },
            {
                $skip: (page - 1) * percentCount4
            },
            {
                $limit: percentCount4
            },
            {
                $project: {
                    _id: 1
                }
            }
        ]);
        console.log("contestVideos4 : ", contestVideos4);
    }
    console.log(contestVideos1, 
        contestVideos2, 
        contestVideos3, 
        contestVideos4);
    // fetch videos on ids collected.
    allContestVideo = await model.find(
        {_id: {
            $in: [...contestVideos1, 
                ...contestVideos2, 
                ...contestVideos3, 
                ...contestVideos4]
            }
        }
    ); 
    return allContestVideo;
}