/**
 * 
 * @param m Math
 * @param d Date
 * @param h number
 * @param s function
 * @returns unique id
 */
export const ObjectId = (
    m = Math,
    d = Date,
    h = 16,
    s = s => m.floor(s).toString(h)
) => s(d.now() / 1000) + ' '.repeat(h).replace(/./g, () => s(m.random() * h));
/**
 * 
 * @param songID unique id
 * @returns object
 */
export const getSongIDAndType = (songID: string) => {
    let type = null;
    let id = null;

    if (!songID) {
        return {
            songID,
            type,
            id,
        };
    }
    // check if id starts with string local then split on - and get element on 1st position in array and assign it to id
    if (songID.startsWith('local')) {
        type = 'local';
        const idArray = songID.split('-');
        const availableID = idArray[1] ? idArray[1] : null;
        if (availableID) { // if availableID
            id = songID;
        } else { // if not available then create a one
            id = 'local-' + ObjectId();
        }
    } else if (songID.startsWith('camera')) { // check if id starts with string camera then split on - and get element on 1st position in array and assign it to id
        type = 'camera';
        const idArray = songID.split('-');
        const availableID = idArray[1] ? idArray[1] : null;
        if (availableID) { // if availableID
            id = songID;
        } else { // if not available then create a one
            id = 'camera-' + ObjectId();
        }
    } else if (songID.startsWith('db')) { // check if id starts with string db then split on - and pop the last element and assign it to id
        type = 'db';
        id = songID.split('-').pop();
    } else if (songID.startsWith('spotify')) { // check if id starts with string db then split on - and pop the last element and assign it to id
        type = 'spotify';
        id = songID.split('-').pop();
    } else {
        type = 'unknown';
        id = songID;
    }
    return {
        songID,
        type,
        id,
    };
};
export const formatSong = (
    id: string,
    name: string,
    thumbnail: string,
    preview_url: string,
    singer: string
) => {
    const music = {
        id,
        name,
        thumbnail: {
            height: 640,
            url: thumbnail,
            width: 640,
        },
        preview_url,
        uri: '',
        artists: [
            {
                external_urls: {
                    spotify: '',
                },
                href: '',
                id: '',
                name: singer,
                type: 'artist',
                uri: '',
            },
        ],
    };

    return music;
};
/**
 * 
 * @param user object
 * @returns string
 */
export const getName = user => {
    const fName = user.firstName;
    const mName = user.middleName;
    const lName = user.lastName;
    if (!fName && !mName && !lName) { // if no fname, mname, lname present then return username
        return user.username;
    } else {
        const firstName = user.firstName ? user.firstName : ' ';
        const middleName = user.middleName ? user.middleName : ' ';
        const lastName = user.lastName ? user.lastName : ' ';
        const name = firstName + ' ' + middleName + ' ' + lastName; // construct name
        return name.trim();  // trim it and return
    }
};
