export const constructName = (userName: string) => {
    // split on space character
    const nameArray = userName.split(' ');
    // triming the splitted array elements
    const filteredArray = nameArray.filter(e => {
        if (e && e !== '') {
            return e.trim();
        }
    });
    const firstName = filteredArray[0] ? filteredArray[0] : '';
    const middleName = filteredArray[1] ? filteredArray[1] : '';
    const lastName = filteredArray[2] ? filteredArray[2] : '';
    // return the name constructed
    return {
        firstName,
        middleName,
        lastName,
    };
};

export const generateUserName = (username = null) => {
    // get 6 digit random number
    const unique = Math.floor(100000 + Math.random() * 900000);
    const isStartWith = username && username.startsWith('@') ? true : false;
    // if a user name starts with @ then return
    if (username && isStartWith) {
        return username;
    }
    // if username doesn't start with @ then append it and return 
    if (username && !isStartWith) {
        return '@' + username;
    }
    // else if username is null then we will create one
    return '@bz' + Math.floor(Math.random() * 100 + 1) + unique;
};

// this will generate 6 digit random number
export const generateOTP = () => Math.floor(100000 + Math.random() * 900000);
