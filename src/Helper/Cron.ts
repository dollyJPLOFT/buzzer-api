import { CronJob } from 'cron';
import UserModel from '../Models/User';
import ContestModel from '../Models/Contest';
// import ContestVote from '../Models/ContestVote';
import VideoModel from '../Models/Video';
import Notification from './Notification';
import schedule from 'node-schedule';
import logger from '../utilis/logger';
import {getRounds} from '../Helper/algoContest';

/**
 * cron job class to schedule contest and declare winners automatically
 */
class Cron {
    cronJob: CronJob;
    contestEndTime: any;
    hasStarted: boolean;
    constructor() {
        // run cron job everyday at 7 pm daily and will changes as per time zone provided
        this.cronJob = new CronJob(
            '0 19 * * *',
            async () => {
                try {
                    this.hasStarted = await this.contest();
                    if (this.hasStarted) {
                        console.log('started: ', new Date(Date.now()));
                        logger.info('started: ', new Date(Date.now()));
                        // voting will end in 9 minutes.
                        this.contestEndTime = new Date(Date.now() + 9*60*1000);
                        const job = schedule.scheduleJob(this.contestEndTime, async () => {
                            console.log('The contest ends', this.contestEndTime);
                            logger.info('The contest ends', this.contestEndTime);
                            await this.winner();
                        });
                    }
                } catch (e) {
                    console.error(e);
                }
            },
            null,
            true,
            'America/Los_Angeles'
        );

        // Start job
        if (!this.cronJob.running) {
            this.cronJob.start();
        }
    }

    contest = async () => {
        // get all contest videos uploaded in between contest ends and contest starts time. 
        // get all top 8 liked uploaded videos in contest
        const videos = await VideoModel.aggregate([
            {
                $match: {
                    $and: [
                        {contest: 1},
                        {createdAt: {
                            $gte: new Date(Date.now() - (24 * 60 * 60 * 1000 - 9*60*1000))
                        }}
                    ]
                }
            }, 
            {
                $project: {
                    userLikedCount: {
                        $size: '$userLiked'
                    },
                    originalVideo: 1,
                    isProcessed: 1,
                    isFlaged: 1,
                    likeCount: 1,
                    shareCount: 1,
                    viewCount: 1,
                    followCount: 1,
                    commentCount: 1,
                    thumbnails: 1,
                    myLikeStatus: 1,
                    myDislikeStatus: 1,
                    myShareStatus: 1,
                    myFollowStatus: 1,
                    songName: 1,
                    songID: 1,
                    postedbyId: 1,
                    postedByname: 1,
                    followuser: 1,
                    isUserVerified: 1,
                    description: 1,
                    dislikeCount: 1,
                    profileImage: 1,
                    videoType: 1,
                    uploader: 1,
                    comments: 1,
                    availableVideos: 1,
                    availableAudios: 1,
                    tags: 1,
                    category: 1,
                    userLiked: 1,
                    userDisliked: 1,
                    userCommented: 1,
                    userShared: 1,
                    userFollwed: 1,
                    userBlocked: 1,
                    userViewed: 1,
                    mentions: 1,
                    hlsUrl: 1,
                    allowDuet: 1,
                    allowTriplet: 1,
                    hiddenByUsers: 1,
                }
            }, 
            {
                $sort: {
                    'userLikedCount': -1
                }
            }, 
            {
                $limit: 8
            }
        ]);
        // videos uploded length is less than 8 then do not create contest.
        if (videos.length < 8) {
            // in case contest is not created then update the videos from contest to normal videos
            await VideoModel.updateMany(
                {contest: 1},
                {contest: 2}
            );
            logger.info(videos.length);
            console.log('There is no video to create contest of day.');
            return false;
        }
        // get all rounds to be shown in ui for a contest
        const rounds = await getRounds(videos);
        let contest = {
            timer: 5,
            videos: [],
            winningPrize: 500,
            startDate: Date.now(),
            round1: rounds['round1'],
            round2: rounds['round2'],
            round3: rounds['round3']
        };
        let start = 'A'.charCodeAt(0);
        // console.log('video : ', video);
        for (let i = 0; i < videos.length; i++) {
            contest.videos.push({
                videoId: videos[i]._id,
                group: Math.ceil((i + 1) / 2),
                uploader: videos[i].uploader,
                position: String.fromCharCode(start + i),
            });
        }
        console.log(`Contest created on ${new Date()}`);
        await new ContestModel(contest).save();

        let users = await UserModel.find();
        users.forEach(user => {
            // send notification
            Notification.send(user._id, {
                title: 'Contest started',
                body: 'Today contest started',
                intent: 'contest',
                targetID: user._id,
                targetUser: user._id,
                otherUserID: '',
                tokenUserID: user._id
            });
        });
        return true;
    };

    winner = async () => {
        // get most recent contest
        const contest: any = await ContestModel.findOne({winnerDeclared: false}).sort([['updatedAt', -1]]);
        // let winner;
        // let videoId;
        // let vote = 0;
        // let participated = [];
        // let videosIds = [];
        if (!contest) {
            throw new Error('No contest has been made');
        }
        contest.endDate = new Date();
        contest.winnerDeclared = true;
        await contest.save();
        // after we finalised winner update videos from contest to normal
        await VideoModel.updateMany(
            {
                $and: [
                    {contest: 1}
                ]
            },
            {contest: 2}
        );
        // videos with most likes is the winner
        await VideoModel.findByIdAndUpdate(contest.videos[0].videoId, {
            contestWon: true
        });
        let users = await UserModel.find();
        users.forEach(user => {
            Notification.send(user._id, {
                title: 'Winner',
                body: 'Today contest winner is finalized',
                intent: 'winner',
                targetID: user._id,
                targetUser: user,
                otherUserID: '',
                tokenUserID: user._id,
            });
        });
    };
}

const cron = new Cron();