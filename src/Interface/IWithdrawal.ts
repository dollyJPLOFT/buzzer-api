import { Document } from 'mongoose';
export interface IWithdrawal extends Document {
    userID: string;
    balance: number;
    accountHolder: string;
    accountNumber: number;
    IFSCcode: string;
    bankName: string;
    createdAt: string;
    updatedAt: string;
    status: string;
    updatedBy: string;
}