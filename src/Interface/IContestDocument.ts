import { Document } from 'mongoose';
export interface IContestDocument extends Document {
    timer: number;
    videos: [Object];
    winningPrize: number;
    winnerUserId: string;
    winningDate: string;
    winnerDeclared: boolean;
    contestName: string;
    createdBy: string;
    createdById: string;
    createdByName: string;
    createdByFullName: string;
    participants: Number;
    startDate: Date;
    endDate: Date;
    round1: [];
    round2: [];
    round3: [];
}
export interface IContestVote extends Document {
    contestId: string;
    voterId: string;
    createdAt: string;
    updatedAt: string;
}
