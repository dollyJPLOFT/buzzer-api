import { Document } from 'mongoose';
export interface IInterest extends Document {
    name: string;
    image: string;
    createdAt: string;
    updatedAt: string;
}
