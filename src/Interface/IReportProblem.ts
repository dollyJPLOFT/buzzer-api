import { Document } from 'mongoose';
export interface IReportProblem extends Document {
    userID: string;
    description: string;
    createdAt: string;
    updatedAt: string;
}