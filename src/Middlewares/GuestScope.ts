import { Response, NextFunction } from 'express';

// for guest login
const guestAllow = (req: any, res: Response, next: NextFunction) => {
    req.guest = true;
    next();
};

export default guestAllow;
