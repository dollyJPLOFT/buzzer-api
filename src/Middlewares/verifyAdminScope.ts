import { Response, NextFunction } from 'express';

// for admin scopes
const verifyAdminScope = (req: any, res: Response, next: NextFunction) => {
    const allowedScopes = ['admin'];
    if (allowedScopes.includes(req.scope)) { // if admin is in request scope then allow them
        next();
    } else {
        res.status(403).json({
            status: 403,
            message: 'error',
            data: {
                message: 'You are not allowed to view this resoure',
            },
        });
    }
};

export default verifyAdminScope;
