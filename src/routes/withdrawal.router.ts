import express, { Request, Response, NextFunction } from 'express';
const router = express.Router();
import WithdrawalController from '../Controller/withdrawal/WithdrawalController';
import jwtverify from '../Middlewares/jwtVerify';
import verifyAdminScope from '../Middlewares/verifyAdminScope';

/**
 *  post withdrawal request to admin
 */
router.post(
    '/',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new WithdrawalController(req, res, next).requests();
    }
);

/**
 *  route to update status of withdrawal
 */
router.post(
    '/status',
    jwtverify,
    verifyAdminScope,
    (req: Request, res: Response, next: NextFunction) => {
        new WithdrawalController(req, res, next).setStatus();
    }
);

/**
 *  route to get all users withdrawal requests
 */
router.get(
    '/list',
    jwtverify,
    verifyAdminScope,
    (req: Request, res: Response, next: NextFunction) => {
        new WithdrawalController(req, res, next).index();
    }
);

/**
 *  route to get all withdrawal request made by self
 */
router.get(
    '/self',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new WithdrawalController(req, res, next).selfRequest();
    }
);

export default router;
