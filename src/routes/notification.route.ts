import express, { Request, Response, NextFunction } from 'express';
const router = express.Router();
import NotificationController from '../Controller/Notifications/NotificationController';
import jwtverify from '../Middlewares/jwtVerify';

/**
 *  route to get all notification of a user
 */
router.get(
    '/',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new NotificationController(req, res, next).index();
    }
);

/**
 *  route to get video
 */
router.get(
    '/video',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new NotificationController(req, res, next).videos();
    }
);

/**
 *  route to get version
 */
router.get(
    '/update',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new NotificationController(req, res, next).getVersion();
    }
);

/**
 *  route to set version
 */
router.post(
    '/update',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new NotificationController(req, res, next).setVersion();
    }
);

router.post(
    '/test',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new NotificationController(req, res, next).test();
    }
);

/**
 *  route to add token for sns
 */
router.post(
    '/',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new NotificationController(req, res, next).addToken();
    }
);

/**
 *  route to update notification read status
 */
router.put(
    '/:id',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new NotificationController(req, res, next).update();
    }
);

/**
 *  route to delete all notification
 */
router.delete(
    '/',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new NotificationController(req, res, next).deleteAll();
    }
);

export default router;
