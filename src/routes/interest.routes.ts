import express, { Request, Response, NextFunction } from 'express';
const router = express.Router();
import InterestController from '../Controller/Interest/InterestController';
import jwtverify from '../Middlewares/jwtVerify';

/**
 *  route to get all interests
 */
router.get(
    '/',
    // jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new InterestController(req, res, next).index();
    }
);

/**
 *  route to add interest
 */
router.post(
    '/',
    // jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new InterestController(req, res, next).add();
    }
);
export default router;
