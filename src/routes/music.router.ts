import express, { Request, Response, NextFunction } from 'express';
const router = express.Router();
import SpotifyController from '../Controller/Spotify/SpotifyController';
import jwtverify from '../Middlewares/jwtVerify';
import MusicController from '../Controller/Music/MusicController';

/**
 * route to search for music
 */
router.get(
    '/search',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new SpotifyController(req, res, next).search();
    }
);

/**
 * route to get recommendation of music
 */
router.get(
    '/recommendation',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new SpotifyController(req, res, next).getRecommendation();
    }
);

/**
 * route to get track
 */
router.get(
    '/:id',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new SpotifyController(req, res, next).getTrack();
    }
);

/**
 * route to get song
 */
router.get(
    '/',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new MusicController(req, res, next).getSong();
    }
);

export default router;
