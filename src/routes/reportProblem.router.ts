import express, { Request, Response, NextFunction } from 'express';
import ReportProblemController from '../Controller/Report/ReportProblemController';
const router = express.Router();
import jwtverify from '../Middlewares/jwtVerify';

/**
 *  report a video route
 */
router.post(
    '/',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new ReportProblemController(req, res, next).report();
    }
);

export default router;