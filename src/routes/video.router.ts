import express, { Response, NextFunction } from 'express';
import jwtverify from '../Middlewares/jwtVerify';
import VideoController from '../Controller/Video/VideoController';
import CommentController from '../Controller/Comment/CommentController';
import UploadController from '../Controller/Video/UploadController';
const router = express.Router();

/**
 * get videos with pagination
 */
router.get(
    '/',
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new VideoController(req, res, next).getVideos();
    }
);

/**
 *  search for a video route
 */
router.get(
    '/search',
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new VideoController(req, res, next).search();
    }
);

/**
 * upload video
 */
router.post(
    '/upload',
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        // new VideoController(req, res, next).uploadVideo();
        new UploadController(req, res, next).upload();
    }
);

/**
 *  get number of likes on a video's comment route
 */
router.get(
    '/:videoID/comment/:commentID',
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new CommentController(req, res, next).shwoLike();
    }
);

/**
 *  like a video comment route
 */
router.post(
    '/:videoID/comment/:commentID',
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new CommentController(req, res, next).like();
    }
);

/**
 *  uddate a video's comment route
 */
router.put(
    '/:videoID/comment/:commentID',
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new CommentController(req, res, next).update();
    }
);

/**
 *  delete a video's comment route
 */
router.delete(
    '/:videoID/comment/:commentID',
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new CommentController(req, res, next).delete();
    }
);

/**
 *  add a comment on a video route
 */
router.post(
    '/:id/comment',
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new CommentController(req, res, next).create();
    }
);

/**
 *  route to get comment on a video
 */
router.get(
    '/:id/comment',
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new CommentController(req, res, next).show();
    }
);

/**
 * route to take action on a video
 */
router.post(
    '/:id',
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new VideoController(req, res, next).action();
    }
);

/**
 * route to delete a video
 */
router.delete(
    '/:id',
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new VideoController(req, res, next).delete();
    }
);

/**
 *  route to get video
 */
router.get(
    '/:id',
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new VideoController(req, res, next).getVideo();
    }
);

export default router;
