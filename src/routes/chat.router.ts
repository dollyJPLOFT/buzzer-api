import express, { Request, Response, NextFunction } from 'express';
const router = express.Router();
import ChatController from '../Controller/Chat/ChatController';
import jwtverify from '../Middlewares/jwtVerify';

/**
 * search for a text in chat
 */
router.get(
    '/search',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new ChatController(req, res, next).search();
    }
);

/**
 * get chat in a conversation
 */
router.get(
    '/:id',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new ChatController(req, res, next).show();
    }
);

/**
 *  delete a conversation
 */
router.delete(
    '/:id',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new ChatController(req, res, next).delete();
    }
);

/**
 * add message in a conversation route
 */
router.post(
    '/:id',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new ChatController(req, res, next).message();
    }
);

/**
 *  route to clear chat
 */
router.post(
    '/:id/clear',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new ChatController(req, res, next).clear();
    }
);

/**
 *  route to create chat
 */
router.post(
    '/',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new ChatController(req, res, next).create();
    }
);

/**
 *  route to get chats
 */
router.get(
    '/',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new ChatController(req, res, next).index();
    }
);

export default router;
