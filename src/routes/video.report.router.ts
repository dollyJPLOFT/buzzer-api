import express, { Request, Response, NextFunction } from 'express';
const router = express.Router();
import CategoryController from '../Controller/Video/ReportCategoryController';
import ReportController from '../Controller/Video/ReportController';
import jwtverify from '../Middlewares/jwtVerify';
import verifyAdminScope from '../Middlewares/verifyAdminScope';

/**
 *  get all report categories route
 */
router.get(
    '/category',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new CategoryController(req, res, next).index();
    }
);

/**
 *  add report categories route
 */
router.post(
    '/category',
    jwtverify,
    verifyAdminScope,
    (req: Request, res: Response, next: NextFunction) => {
        new CategoryController(req, res, next).create();
    }
);

/**
 *  get detail about report category route
 */
router.get(
    '/category/:id',
    jwtverify,
    verifyAdminScope,
    (req: Request, res: Response, next: NextFunction) => {
        new CategoryController(req, res, next).show();
    }
);

/**
 *  update report category route
 */
router.put(
    '/category/:id',
    jwtverify,
    verifyAdminScope,
    (req: Request, res: Response, next: NextFunction) => {
        new CategoryController(req, res, next).update();
    }
);

/**
 *  delete report category route
 */
router.delete(
    '/category/:id',
    jwtverify,
    verifyAdminScope,
    (req: Request, res: Response, next: NextFunction) => {
        new CategoryController(req, res, next).delete();
    }
);

/**
 *  get list of report categories route
 */
router.get(
    '/',
    jwtverify,
    verifyAdminScope,
    (req: Request, res: Response, next: NextFunction) => {
        new ReportController(req, res, next).index();
    }
);


export default router;
