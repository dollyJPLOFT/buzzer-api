import express, { Request, Response, NextFunction } from 'express';
import jwtverify from '../Middlewares/jwtVerify';
import ContestController from '../Controller/Contest/ContestController';
import guestAllow from '../Middlewares/GuestScope';
const router = express.Router();

/**
 * get videos with pagination
 */
router.get(
    '/',
    guestAllow,
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new ContestController(req, res, next).getContest();
    }
);

/**
 * vote a video.
 */
router.post(
    '/vote/video',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new ContestController(req, res, next).voteForVideo();
    }
);

/**
 * get current pst date
 */
router.get(
    '/pstTime',
    guestAllow,
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new ContestController(req, res, next).getPstDate();
    }
);

/**
 * get current user
 */
router.get(
    '/userCount',
    guestAllow,
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new ContestController(req, res, next).getParticipateUserCount();
    }
);

/**
 * put eleminate user
 */
router.put(
    '/eliminate',
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new ContestController(req, res, next).eliminate();
    }
);

/**
 *  route to participate
 */
router.put(
    '/enter',
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new ContestController(req, res, next).participate();
    }
);

/**
 * Winer list
 */
router.get(
    '/winner',
    guestAllow,
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new ContestController(req, res, next).winnerList();
    }
);

/**
 * Top creater
 */
router.get(
    '/top/creator',
    guestAllow,
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new ContestController(req, res, next).topCreator();
    }
);

/**
 * Top viewer
 */
router.get(
    '/top/viewer',
    guestAllow,
    jwtverify,
    async (req: any, res: Response, next: NextFunction) => {
        new ContestController(req, res, next).topViewer();
    }
);

export default router;
