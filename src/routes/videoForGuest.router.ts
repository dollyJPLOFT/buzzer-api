import express, { Response, NextFunction } from 'express';
import VideoControllerForGuest from '../Controller/Video/videoControllerForGuest';
const router = express.Router();

/**
 * routes for guest userUnfollwed
 */
router.get('/', async (req: any, res: Response, next: NextFunction) => {
    new VideoControllerForGuest(req, res, next).getVideos();
});

router.get('/search', async (req: any, res: Response, next: NextFunction) => {
    new VideoControllerForGuest(req, res, next).search();
});

router.get('/:id', async (req: any, res: Response, next: NextFunction) => {
    new VideoControllerForGuest(req, res, next).getVideo();
});

export default router;
