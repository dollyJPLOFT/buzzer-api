import express, { Request, Response, NextFunction } from 'express';
const router = express.Router();
import WalletController from '../Controller/Wallet/WalletController';
import jwtverify from '../Middlewares/jwtVerify';
import verifyAdminScope from '../Middlewares/verifyAdminScope';

/**
 * route to wallet balance
 */
router.get(
    '/',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new WalletController(req, res, next).show();
    }
);

/**
 *  route to add amount to wallet
 */
router.post(
    '/',
    jwtverify,
    verifyAdminScope,
    (req: Request, res: Response, next: NextFunction) => {
        new WalletController(req, res, next).add();
    }
);

export default router;
