import express, { Request, Response, NextFunction } from 'express';
const router = express.Router();
import PrivacyController from '../Controller/Privacy/PrivacyController';
import jwtverify from '../Middlewares/jwtVerify';

/**
 *  get privacy of a user route
 */
router.get(
    '/',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new PrivacyController(req, res, next).show();
    }
);

/**
 *  update privacy of a user route
 */
router.put(
    '/',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new PrivacyController(req, res, next).update();
    }
);

export default router;
