import express, { Request, Response, NextFunction } from 'express';
const router = express.Router();
import TransactionController from '../Controller/Transaction/TransactionController';
import PaytmController from '../Controller/Transaction/PaytmController';
import jwtverify from '../Middlewares/jwtVerify';

/**
 *  buy crown route
 */
router.post(
    '/',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new TransactionController(req, res, next).buy();
    }
);

/**
 *  generate paytm check sum route
 */
router.post(
    '/checksum',
    jwtverify,
    (req: Request, res: Response, next: NextFunction) => {
        new PaytmController(req, res, next).getToken();
    }
);

export default router;
