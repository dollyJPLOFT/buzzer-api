import { Request, Response, NextFunction } from 'express';
import Withdrawal from '../../Models/Withdrawal';
import User from '../../Models/User';

/**
 * Wallet Controller Class
 *  @author Dolly Garg <dollygarg137@gmail.com>
 */
class WithdrawalController {
    req: any;
    res: Response;
    next: NextFunction;

    /**
     * Constructor
     * @param req express.Request
     * @param res express.Response
     * @param next express.NextFunction
     */

    constructor(req: Request, res: Response, next: NextFunction) {
        this.req = req;
        this.res = res;
        this.next = next;
    }

    requests = async () => {
        try {
            const userID = this.req.uid;
            const {balance, accountHolder, accountNumber, IFSCcode, bankName} = this.req.body; // destructure
            if (!balance)
                throw new Error('Please provide balance in request body');
            if (!accountHolder)
                throw new Error('Please provide accountHolder in request body');
            if (!accountNumber)
                throw new Error('Please provide accountNumber in request body');
            if (!IFSCcode)
                throw new Error('Please provide IFSCcode in request body');
            if (!bankName)
                throw new Error('Please provide bankName in request body');

            // check if balance is less then earning
            const user = await User.findById(userID);
            if (!user) throw new Error('Invalid request');
            if (+(user.earning) < +(balance)) throw new Error("Invalid request, Amount can't be greater than earning");
            user.earning = +(user.earning) - +balance; // reduce balance from earning
            await user.save() // update earning
            // save request
            const doc = await new Withdrawal({userID, ...this.req.body}).save(); // make new withdrawal request
            this.res.status(200).json({
                status: 200,
                message: 'success',
                data: {
                    message: 'Wthdrawal request has been sent',
                    doc,
                },
            });
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    transaction: {},
                },
            });
        }
    };

    setStatus = async () => {
        try {
            const adminId = this.req.uid;
            const { withdrawalId, status } = this.req.body; // destructure
            if (!withdrawalId)
                throw new Error('Please provide withdrawalId in request body');
            // update request
            const doc = await Withdrawal.findById(withdrawalId);
            if (!doc)
                throw new Error('Invalid withdrawalId in request body');
            const user = await User.findById(doc.userID);
            if (!user) throw new Error('Invalid request');
            if (status === 'Approved') {
            } else if (status === 'Rejected') { // in case of reject, reset the balance to prior the request
                // update user earning
                user.earning = +user.earning + +doc.balance; 
            }
            // save user
            await user.save();
            doc.status = status;
            doc.updatedBy = adminId;
            const doc_ = await doc.save();
            this.res.status(200).json({
                status: 200,
                message: 'success',
                data: {
                    message: `Request has been ${status}`,
                    doc_,
                },
            });
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    transaction: {},
                },
            });
        }
    }

    selfRequest = async () => {
        try {
            const userID = this.req.uid;
            const limit =
                this.req.query.limit && parseInt(this.req.query.limit) !== 0
                    ? this.req.query.limit
                    : 2;
            const page =
                this.req.query.page && parseInt(this.req.query.page) !== 0
                    ? this.req.query.page
                    : 1;
            // get user requests
            const query = {userID: userID};
            const requests = await Withdrawal.find(query)
            .skip(parseInt(limit) * (parseInt(page) - 1))
            .limit(parseInt(limit))
            .sort([['updatedAt', -1]]);
            const count = await Withdrawal.countDocuments(query);
            const total = Math.ceil(count / limit);

            this.res.status(200).json({
                status: 200,
                message: 'withdrawal requests list',
                data: {
                    message: 'withdrawal requests list',
                    requests
                },
                pagination: {
                    total,
                    page: parseInt(page),
                    next:
                        parseInt(page) < total
                            ? parseInt(page) + 1
                            : null,
                    prev:
                        parseInt(page) <= total && parseInt(page) !== 1
                            ? parseInt(page) - 1
                            : null,
                },
            });
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    transaction: {},
                },
            });
        }
    }

    index = async () => {
        try {
            const limit =
                this.req.query.limit && parseInt(this.req.query.limit) !== 0
                    ? this.req.query.limit
                    : 2;
            const page =
                this.req.query.page && parseInt(this.req.query.page) !== 0
                    ? this.req.query.page
                    : 1;
            // get all users withdrawal requests
            const requests = await Withdrawal.find()
            .skip(parseInt(limit) * (parseInt(page) - 1))
            .limit(parseInt(limit))
            .sort([['updatedAt', -1]]);
            const count = await Withdrawal.countDocuments();
            const total = Math.ceil(count / limit);

            this.res.status(200).json({
                status: 200,
                message: 'withdrawal requests list',
                data: {
                    message: 'withdrawal requests list',
                    requests
                },
                pagination: {
                    total,
                    page: parseInt(page),
                    next:
                        parseInt(page) < total
                            ? parseInt(page) + 1
                            : null,
                    prev:
                        parseInt(page) <= total && parseInt(page) !== 1
                            ? parseInt(page) - 1
                            : null,
                },
            });
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    transaction: {},
                },
            });
        }
    }
}

export default WithdrawalController;
