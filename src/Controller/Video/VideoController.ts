import { Request, Response, NextFunction } from 'express';
import s3 from '../../Helper/DoSpace';
import VideoModel from '../../Models/Video';
import UserModel from '../../Models/User';
import CommentModel from '../../Models/Comment';
import logger from '../../utilis/logger';
import { v4 as uuidv4 } from 'uuid';
import Spotify from '../Spotify/Spotify';
import SpotifyModel from '../../Models/Spotify';
import moment from 'moment';
import FavoriteModel from '../../Models/Favorite';
import PrivacyModel from '../../Models/Privacy';
import Notification from '../../Helper/Notification';
import { getName } from '../../Helper/utilis';
import { videosFlow } from '../../Helper/algo';
class VideoController {
    req: any;
    res: Response;
    next: NextFunction;
    bucket: any;

    /**
     * Constructor
     * @param req express.Request
     * @param res  express.Response
     * @param next   express.NextFunction
     */

    constructor(req: Request, res: Response, next: NextFunction) {
        this.req = req;
        this.res = res;
        this.next = next;
        this.bucket = process.env.DIGITAL_OCEAN_SPACE_NAME
            ? process.env.DIGITAL_OCEAN_SPACE_NAME
            : 'buzzer';
    }

    getPrivateUsers = async uid => {
        // get private user list
        const requestUser = await UserModel.findById(uid);
        const requestUserFollowingUsers: any = requestUser.follwingUsers;
        const privacyQuery = {
            postIsPublic: false,
        };
        const privacy = await PrivacyModel.find(privacyQuery);
        let privateUserProfile = [];
        privacy.forEach(element => {
            if (!requestUserFollowingUsers.includes(element.user)) {
                privateUserProfile.push(element.user.toString());
            }
        });
        return privateUserProfile;
    };

    getToken = async () => {
        let access_token: string;

        let doc: any = await SpotifyModel.findOne({
            isManuallyAdded: true,
        });
        const refresh_token = doc.refresh_token;
        const expires_in = doc.expires_in;
        const generated_at = doc.generated_at;
        const now = moment().unix();
        if (now <= parseInt(generated_at) + parseInt(expires_in)) {
            access_token = doc.access_token;
        } else {
            // console.log('jjj');
            const tokens = await Spotify.refreshToken(refresh_token);
            // console.log(' tokens', tokens);
            access_token = tokens.access_token;
            doc = await SpotifyModel.findOneAndUpdate(
                { _id: doc.id },
                {
                    ...tokens,
                    access_token: tokens.access_token,
                    refresh_token: tokens.refresh_token
                        ? tokens.refresh_token
                        : doc.refresh_token,
                    generated_at: moment().unix(),
                },
                { upsert: true, new: true, setDefaultsOnInsert: true }
            );
        }

        // console.log(doc);

        return access_token;
    };

    formatVideos = async (videos, uid) => {
        let data: any = [];
        // set mylike status and follower users etc for every video
        if (videos.length >= 1) {
            for (const video of videos) {
                const userLiked = video.userLiked;
                if (userLiked.includes(uid)) {
                    video.myLikeStatus = true;
                } else {
                    video.myLikeStatus = false;
                }
                const uploader: any = await UserModel.findById(video.uploader);
                const uploaderFollwers: string[] = uploader
                    ? uploader.follwersUsers
                    : [];

                if (uploaderFollwers.length >= 1) {
                    if (uploaderFollwers.includes(uid)) {
                        video.myfollwStatus = true;
                        video.followuser = true;
                    } else {
                        video.myfollwStatus = false;
                        video.followuser = false;
                    }
                } else {
                    video.myfollwStatus = false;
                    video.followuser = false;
                }

                video.postedByName = uploader.username;
                video.profileImage = uploader.image;
                video.isUserVerified = uploader.isUserVerified;

                data.push(video);
            }
        }

        return data;
    };

    search = async () => {
        try {
            const { q } = this.req.query;
            const uid = this.req.uid;
            if (!q) throw new Error('please provide ?q=');
            // search for q in videos for songname, description and user name,
            // whole profile is not blocked and have not kept there profile private
            const privateUserProfile = await this.getPrivateUsers(uid);
            // pagination
            const limit =
                this.req.query.limit && parseInt(this.req.query.limit) !== 0
                    ? this.req.query.limit
                    : 2;
            const page =
                this.req.query.page && parseInt(this.req.query.page) !== 0
                    ? this.req.query.page
                    : 1;
            const user = await UserModel.findById(uid);
            const userWhoBlockedMe = user.usersBlockedMe
                ? user.usersBlockedMe
                : [];

            console.log(' userWhoBlockedMe', userWhoBlockedMe);
            const query = {
                $or: [
                    { songname: { $regex: q, $options: 'i' } },
                    { description: { $regex: q, $options: 'i' } },
                    { postedByName: { $regex: q, $options: 'i' } },
                ],
                uploader: {
                    $nin: [uid, ...privateUserProfile, ...userWhoBlockedMe],
                },
            };
            const result = await VideoModel.find(query)
                .select('id  originalVideo thumbnails ')
                .skip(parseInt(limit) * (parseInt(page) - 1))
                .limit(parseInt(limit))
                .sort([['createdAt', -1]]);
            const count = await VideoModel.countDocuments(query);
            const total = Math.ceil(count / limit);
            this.res.status(200).json({
                status: 200,
                message: 'success',
                data: {
                    message: 'serach result',
                    result,
                },
                pagination: {
                    total,
                    page: parseInt(page),
                    next: parseInt(page) < total ? parseInt(page) + 1 : null,
                    prev:
                        parseInt(page) <= total && parseInt(page) !== 1
                            ? parseInt(page) - 1
                            : null,
                },
            });
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    result: [],
                },
            });
        }
    };

    action = async () => {
        try {
            const { action } = this.req.body; // destructure
            const { id } = this.req.params; // destructure
            const uid = this.req.uid;
            const video: any = await VideoModel.findById(id)
                .select('-password')
                .populate('uploader');
            if (!video) throw new Error('Video id is invalid');
            const uploader = video.uploader;
            const user: any = await UserModel.findById(uid);
            const isAlreadyLiked = user.likedVideos
                ? user.likedVideos.includes(id)
                : false;
            const isAlreadyDisliked = user.dislikedVideos
                ? user.dislikedVideos.includes(id)
                : false;

            const isUploaderBlocked = user.blockedUsers.includes(uploader._id);
            const isUploaderFollowed = user.follwingUsers.includes(
                uploader._id
            );
            const isUploaderAndUserSame =
                uploader.id.toString() === uid.toString();

            console.log('isUploaderAndUserSame', isUploaderAndUserSame);

            let name = getName(user);

            console.log(name);

            switch (action) {
                case 'hide':
                    // hide a video and save into user videoHiddenArray
                    user.videosHidden.push(video);
                    // hide a video and save into video hiddenByUsers
                    video.hiddenByUsers.push(user);
                    await user.save();
                    await video.save();
                    this.res.status(200).json({
                        status: 200,
                        message: 'success',
                        data: {
                            message: 'You have hidden this video',
                        },
                    });
                    break;
                case 'like':
                    // in case user already liked the video
                    if (isAlreadyLiked) {
                        user.dislikedVideos.pull(video);
                        video.userDisliked.pull(user);
                        video.myLikeStatus = true;
                        await video.save();
                        await video.save();
                        this.res.status(200).json({
                            status: 200,
                            message: 'success',
                            data: {
                                message: 'You have liked this video',
                            },
                        });
                    } else {
                        // in case user has not liked the video before
                        user.likedVideos.push(video);
                        video.userLiked.push(user);
                        video.myLikeStatus = true;
                        user.dislikedVideos.pull(video);
                        video.userDisliked.pull(user);
                        await user.save();
                        await video.save();
                        // send notification if uploader and user are not same
                        if (!isUploaderAndUserSame) {
                            await Notification.send(uploader, {
                                title: name,
                                body: name + ' liked your video',
                                intent: 'video-liked',
                                targetID: video._id,
                                targetUser: user,
                                otherUserID: uploader._id,
                                tokenUserID: uid,
                                imageUrl: video.thumbnails,
                            });
                        }

                        this.res.status(200).json({
                            status: 200,
                            message: 'success',
                            data: {
                                message: 'You have liked this video',
                            },
                        });
                    }

                    break;

                case 'dislike':
                    // in case user has liked the video before
                    if (isAlreadyDisliked) {
                        console.log(1);
                        user.likedVideos.pull(video);
                        video.userLiked.pull(user);
                        video.myLikeStatus = false;
                        await user.save();
                        await video.save();

                        this.res.status(200).json({
                            status: 200,
                            message: 'success',
                            data: {
                                message: 'You have disliked this video',
                            },
                        });
                    } else {
                        // in case user has not liked the video before
                        console.log(2);
                        user.likedVideos.pull(video);
                        user.dislikedVideos.push(video);
                        video.userLiked.pull(user);
                        video.userDisliked.push(user);
                        video.myLikeStatus = false;
                        await user.save();
                        await video.save();
                        this.res.status(200).json({
                            status: 200,
                            message: 'success',
                            data: {
                                message: 'You have disliked this video',
                            },
                        });
                    }

                    break;

                case 'share':
                    // share the video and save it user sharedVideos and video's userShared array.
                    user.sharedVideos.push(video);
                    video.userShared.push(user);
                    video.myShareStatus = true;
                    await user.save();
                    await video.save();
                    this.res.status(200).json({
                        status: 200,
                        message: 'success',
                        data: {
                            message: 'You have shared this video',
                        },
                    });
                    break;

                case 'follow':
                    // follow the uploader
                    if (isUploaderBlocked)
                        throw new Error(
                            'You have blocked this user please unblock first'
                        );

                    if (isUploaderFollowed)
                        throw new Error('You are already following this user');
                    user.follwingUsers.push(uploader);
                    user.isFollow = true;
                    uploader.follwersUsers.push(user);
                    video.myfollwStatus = true;
                    video.userFollowed.push(user);
                    if (!isUploaderAndUserSame) {
                        // send notification
                        await Notification.send(uploader, {
                            title: name,
                            body: name + ' started followinfg you',
                            intent: 'user',
                            targetID: uid,
                            targetUser: user,
                            otherUserID: uploader._id,
                            tokenUserID: uid,
                            imageUrl: video.thumbnails,
                        });
                    }

                    await user.save();
                    await uploader.save();
                    await video.save();
                    this.res.status(200).json({
                        status: 200,
                        message: 'success',
                        data: {
                            message: 'You are now following this user',
                        },
                    });

                    break;

                case 'unfollow':
                    // unfollow the uploader
                    if (isUploaderBlocked)
                        throw new Error(
                            'You have blocked this user please unblock first'
                        );

                    user.follwingUsers.pull(uploader);
                    user.isFollow = false;
                    uploader.follwersUsers.pull(user);
                    video.myfollwStatus = false;
                    video.userFollowed.pull(user);
                    await user.save();
                    await uploader.save();
                    await video.save();
                    this.res.status(200).json({
                        status: 200,
                        message: 'success',
                        data: {
                            message: 'You have unfollowed this user',
                        },
                    });

                    break;

                case 'block':
                    // block the uploader
                    if (isUploaderBlocked)
                        throw new Error('You have already blocked this user');

                    user.blockedUsers.push(uploader);
                    await user.save();
                    this.res.status(200).json({
                        status: 200,
                        message: 'success',
                        data: {
                            message: 'You have blocked this user',
                        },
                    });

                    break;

                case 'unblock':
                    // unblock the uploader
                    user.blockedUsers.pull(uploader);
                    await user.save();
                    this.res.status(200).json({
                        status: 200,
                        message: 'success',
                        data: {
                            message: 'You have unblocked this user',
                        },
                    });

                    break;

                case 'view+':
                    // view the video and add it to user viewed video and video user viewed array.
                    user.viewedVideos.push(video);
                    video.userViewed.push(user);
                    // save the update
                    await user.save();
                    const videoSaved = await video.save();
                    this.res.status(200).json({
                        status: 200,
                        message: 'success',
                        data: {
                            message: 'You have viewed this video',
                            video: videoSaved,
                        },
                    });
                    break;

                case 'share+':
                    // share the video and add it to user shared video and video user shared array.
                    user.sharedVideos.push(video);
                    video.userShared.push(user);
                    // save the update
                    await user.save();
                    await video.save();
                    this.res.status(200).json({
                        status: 200,
                        message: 'success',
                        data: {
                            message: 'You have shared this video',
                        },
                    });

                    break;

                default:
                    this.res.status(200).json({
                        status: 400,
                        message: 'error',
                        data: {
                            message:
                                'Please provide action like, share, follow, unfollow, block and unblock',
                        },
                    });
                    break;
            }
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                },
            });
        }
    };

    getVideos = async () => {
        try {
            const q = this.req.query.q; // q => contest
            const q1 = this.req.query.q1; // q1 => buzzing
            const uid = this.req.uid;
            // pagination
            const limit =
                this.req.query.limit && parseInt(this.req.query.limit) !== 0
                    ? this.req.query.limit
                    : 2;
            const page =
                this.req.query.page && parseInt(this.req.query.page) !== 0
                    ? this.req.query.page
                    : 1;
            let user;
            let count;
            let total;
            let query;
            let sort = 'createdAt';
            if (q1) {
                sort = q1 == 'buzzing' ? 'userLiked' : 'createdAt';
            }

            const privateUserProfile = await this.getPrivateUsers(uid);
            user = await UserModel.findById(uid);
            const userWhoBlockedMe = user.usersBlockedMe
                ? user.usersBlockedMe
                : [];

            console.log(' userWhoBlockedMe', userWhoBlockedMe);
            console.log(privateUserProfile);

            // get hidden videos of user
            const videosHidden = user.videosHidden ? user.videosHidden : [];

            switch (q) {
                case 'liked':
                    // get all liked videos which are not in hidden vides and
                    //  not uploaded by private user and blocked user and login user.
                    query = {
                        userLiked: uid,
                        _id: {
                            $nin: [...videosHidden]
                        },
                        uploader: {
                            $nin: [
                                uid,
                                ...privateUserProfile,
                                ...userWhoBlockedMe,
                            ],
                        },
                    };
                    const likedVideos = await VideoModel.find(query)
                        // .select('id  originalVideo thumbnails ')
                        .skip(parseInt(limit) * (parseInt(page) - 1))
                        .limit(parseInt(limit))
                        .sort([['createdAt', -1]]);
                    count = await VideoModel.countDocuments(query);
                    total = Math.ceil(count / limit);

                    this.res.status(200).json({
                        status: 200,
                        message: 'success',
                        data: {
                            message: 'video list',
                            videos: await this.formatVideos(likedVideos, uid),
                        },
                        pagination: {
                            total,
                            page: parseInt(page),
                            next:
                                parseInt(page) < total
                                    ? parseInt(page) + 1
                                    : null,
                            prev:
                                parseInt(page) <= total && parseInt(page) !== 1
                                    ? parseInt(page) - 1
                                    : null,
                        },
                    });
                    break;

                case 'shared':
                    // get all shared videos which are not in hidden vides and
                    // not uploaded by private user and blocked user and login user.
                    query = {
                        userShared: uid,
                        _id: {
                            $nin: [...videosHidden]
                        },
                        uploader: {
                            $nin: [
                                uid,
                                ...privateUserProfile,
                                ...userWhoBlockedMe,
                            ],
                        },
                    };
                    const sharedVideos = await VideoModel.find(query)
                        //  .select('id  originalVideo thumbnails ')
                        .skip(parseInt(limit) * (parseInt(page) - 1))
                        .limit(parseInt(limit))
                        .sort([['createdAt', -1]]);
                    count = await VideoModel.countDocuments(query);
                    total = Math.ceil(count / limit);
                    this.res.status(200).json({
                        status: 200,
                        message: 'success',
                        data: {
                            message: 'video list',
                            videos: await this.formatVideos(sharedVideos, uid),
                        },
                        pagination: {
                            total,
                            page: parseInt(page),
                            next:
                                parseInt(page) < total
                                    ? parseInt(page) + 1
                                    : null,
                            prev:
                                parseInt(page) <= total && parseInt(page) !== 1
                                    ? parseInt(page) - 1
                                    : null,
                        },
                    });

                    break;

                case 'viewed':
                    // get all viewed videos which are not in hidden vides and
                    //  not uploaded by private user and blocked user and login user.
                    query = {
                        userViewed: uid,
                        _id: {
                            $nin: [...videosHidden]
                        },
                        uploader: {
                            $nin: [
                                uid,
                                ...privateUserProfile,
                                ...userWhoBlockedMe,
                            ],
                        },
                    };
                    const viewedVideos = await VideoModel.find(query)
                        // .select('id  originalVideo thumbnails ')
                        .skip(parseInt(limit) * (parseInt(page) - 1))
                        .limit(parseInt(limit))
                        .sort([['createdAt', -1]]);
                    count = await VideoModel.countDocuments(query);
                    total = Math.ceil(count / limit);
                    this.res.status(200).json({
                        status: 200,
                        message: 'success',
                        data: {
                            message: 'video list',
                            videos: await this.formatVideos(viewedVideos, uid),
                        },
                        pagination: {
                            total,
                            page: parseInt(page),
                            next:
                                parseInt(page) < total
                                    ? parseInt(page) + 1
                                    : null,
                            prev:
                                parseInt(page) <= total && parseInt(page) !== 1
                                    ? parseInt(page) - 1
                                    : null,
                        },
                    });

                    break;

                case 'uploaded':
                    // get all videos uploaded by logined user
                    query = {
                        uploader: uid,
                    };
                    const uploadedVideos = await VideoModel.find(query)
                        // .select('id  originalVideo thumbnails ')
                        .skip(parseInt(limit) * (parseInt(page) - 1))
                        .limit(parseInt(limit))
                        .sort([['createdAt', -1]]);
                    count = await VideoModel.countDocuments(query);
                    total = Math.ceil(count / limit);

                    this.res.status(200).json({
                        status: 200,
                        message: 'success',
                        data: {
                            message: 'video list',
                            videos: await this.formatVideos(
                                uploadedVideos,
                                uid
                            ),
                        },
                        pagination: {
                            total,
                            page: parseInt(page),
                            next:
                                parseInt(page) < total
                                    ? parseInt(page) + 1
                                    : null,
                            prev:
                                parseInt(page) <= total && parseInt(page) !== 1
                                    ? parseInt(page) - 1
                                    : null,
                        },
                    });

                    break;

                case 'following':
                    // get all following user videos which are not in hidden vides and
                    // not uploaded by private user and blocked user and login user.
                    const following = user ? user.follwingUsers : [];
                    // now get videos where uploader in following
                    query = {
                        _id: {
                            $nin: [...videosHidden]
                        },
                        uploader: {
                            $in: following,
                            $nin: [
                                uid,
                                ...privateUserProfile,
                                ...userWhoBlockedMe,
                            ],
                        },
                    };
                    const followingUsersVideos = await VideoModel.find(query)
                        // .select('id  originalVideo thumbnails ')
                        .skip(parseInt(limit) * (parseInt(page) - 1))
                        .limit(parseInt(limit))
                        .sort([['createdAt', -1]]);

                    count = await VideoModel.countDocuments(query);
                    total = Math.ceil(count / limit);
                    this.res.status(200).json({
                        status: 200,
                        message: 'success',
                        data: {
                            message: 'video list',
                            videos: await this.formatVideos(
                                followingUsersVideos,
                                uid
                            ),
                        },
                        pagination: {
                            total,
                            page: parseInt(page),
                            next:
                                parseInt(page) < total
                                    ? parseInt(page) + 1
                                    : null,
                            prev:
                                parseInt(page) <= total && parseInt(page) !== 1
                                    ? parseInt(page) - 1
                                    : null,
                        },
                    });

                    break;
                case 'followers':
                    const followers = user ? user.follwersUsers : [];
                    query = {
                        _id: {
                            $nin: [...videosHidden]
                        },
                        uploader: {
                            $in: followers,
                            $nin: [
                                uid,
                                ...privateUserProfile,
                                ...userWhoBlockedMe,
                            ],
                        },
                    };
                    const followersVideos = await VideoModel.find(query)
                        //  .select('id  originalVideo thumbnails ')
                        .skip(parseInt(limit) * (parseInt(page) - 1))
                        .limit(parseInt(limit))
                        .sort([['createdAt', -1]]);
                    count = await VideoModel.countDocuments(query);
                    total = Math.ceil(count / limit);
                    this.res.status(200).json({
                        status: 200,
                        message: 'success',
                        data: {
                            message: 'video list',
                            videos: await this.formatVideos(
                                followersVideos,
                                uid
                            ),
                        },
                        pagination: {
                            total,
                            page: parseInt(page),
                            next:
                                parseInt(page) < total
                                    ? parseInt(page) + 1
                                    : null,
                            prev:
                                parseInt(page) <= total && parseInt(page) !== 1
                                    ? parseInt(page) - 1
                                    : null,
                        },
                    });

                    break;

                case 'hashtag':
                    user = await UserModel.findById(uid);
                    const reqQuery = this.req.query.hashtag;
                    query = {
                        _id: {
                            $nin: [...videosHidden]
                        },
                        uploader: {
                            $nin: [
                                user.blockedUsers,
                                uid,
                                ...privateUserProfile,
                                ...userWhoBlockedMe,
                            ],
                        },
                        description: { $regex: reqQuery, $options: 'i' },
                    };
                    const hashTagVideos = await VideoModel.find(query)
                        //  .select('id  originalVideo thumbnails ')
                        .skip(parseInt(limit) * (parseInt(page) - 1))
                        .limit(parseInt(limit))
                        .sort([['createdAt', -1]]);
                    count = await VideoModel.countDocuments(query);
                    console.log(count);
                    total = Math.ceil(count / limit);
                    this.res.status(200).json({
                        status: 200,
                        message: 'success',
                        data: {
                            message: 'hashtag video list',
                            hashtag: reqQuery,
                            videos: await this.formatVideos(hashTagVideos, uid),
                            totalvideos: count,
                        },
                        pagination: {
                            total,
                            page: parseInt(page),
                            next:
                                parseInt(page) < total
                                    ? parseInt(page) + 1
                                    : null,
                            prev:
                                parseInt(page) <= total && parseInt(page) !== 1
                                    ? parseInt(page) - 1
                                    : null,
                        },
                    });

                    break;

                case 'song':
                    user = await UserModel.findById(uid);
                    const songName = this.req.query.songName;
                    const songID = this.req.query.songID;
                    if (songName) {
                        const query = {
                            _id: {
                                $nin: [...videosHidden]
                            },
                            uploader: {
                                $nin: [
                                    user.blockedUsers,
                                    uid,
                                    ...privateUserProfile,
                                    ...userWhoBlockedMe,
                                ],
                            },
                            songName: { $regex: songName, $options: 'i' },
                        };
                        const songVideos = await VideoModel.find(query)
                            //  .select('id  originalVideo thumbnails ')
                            .skip(parseInt(limit) * (parseInt(page) - 1))
                            .limit(parseInt(limit))
                            .sort([['createdAt', -1]]);
                        const count = await VideoModel.countDocuments(query);
                        const total = Math.ceil(count / limit);
                        this.res.status(200).json({
                            status: 200,
                            message: 'success',
                            data: {
                                message: 'by song video list',
                                videoCount: count,
                                music: {},
                                isFavorite: false,
                                videos: await this.formatVideos(
                                    songVideos,
                                    uid
                                ),
                            },
                            pagination: {
                                total,
                                page: parseInt(page),
                                next:
                                    parseInt(page) < total
                                        ? parseInt(page) + 1
                                        : null,
                                prev:
                                    parseInt(page) <= total &&
                                    parseInt(page) !== 1
                                        ? parseInt(page) - 1
                                        : null,
                            },
                        });
                    } else {
                        const query = {
                            _id: {
                                $nin: [...videosHidden]
                            },
                            uploader: {
                                $nin: [
                                    user.blockedUsers,
                                    uid,
                                    ...privateUserProfile,
                                    ...userWhoBlockedMe,
                                ],
                            },
                            songID: songID,
                        };
                        const trackID = songID;
                        const token = await this.getToken();
                        const result = await Spotify.getTrack(trackID, token);
                        const {
                            id,
                            name,
                            preview_url,
                            uri,
                            album,
                            artists,
                        } = result;
                        // get first thumbnail
                        const thumbnail = album.images
                            ? album.images.shift()
                            : {};
                        const music = {
                            id,
                            name,
                            thumbnail,
                            preview_url,
                            uri,
                            artists,
                        };
                        const songVideos = await VideoModel.find(query)
                            //  .select('id  originalVideo thumbnails ')
                            .skip(parseInt(limit) * (parseInt(page) - 1))
                            .limit(parseInt(limit))
                            .sort([['createdAt', -1]]);
                        const count = await VideoModel.countDocuments(query);
                        const total = Math.ceil(count / limit);
                        // get if it is in fovorite
                        const favorite = await FavoriteModel.findOne({
                            songID: songID,
                        });
                        const isFavorite = favorite
                            ? favorite.isFavorite
                            : false;
                        this.res.status(200).json({
                            status: 200,
                            message: 'success',
                            data: {
                                message: 'by song video list',
                                videoCount: count,
                                music,
                                isFavorite,
                                videos: await this.formatVideos(
                                    songVideos,
                                    uid
                                ),
                            },
                            pagination: {
                                total,
                                page: parseInt(page),
                                next:
                                    parseInt(page) < total
                                        ? parseInt(page) + 1
                                        : null,
                                prev:
                                    parseInt(page) <= total &&
                                    parseInt(page) !== 1
                                        ? parseInt(page) - 1
                                        : null,
                            },
                        });
                    }

                    break;
                case 'contest':
                    // get contest videos
                    user = await UserModel.findById(uid);
                    const blockedUser = user ? user.blockedUsers : [];
                    console.log('blocked', blockedUser);
                    console.log('privateUserProfile', privateUserProfile);
                    query = {
                        _id: {
                            $nin: [...videosHidden]
                        },
                        contest: 1
                    };
                    count = await VideoModel.countDocuments(query);
                    let allContestVideo: any;
                    console.log('ggg', query);
                    if (q1 === 'new') {
                        // in case for new
                        allContestVideo = await VideoModel.find(query)
                            .skip(parseInt(limit) * (parseInt(page) - 1))
                            .limit(parseInt(limit))
                            .sort([['createdAt', -1]]);
                        total = Math.ceil(count / limit);
                    } else if (q1 === 'buzzing') {
                        // in case of buzzing algo is implemented
                        allContestVideo = await videosFlow(query, limit, page, VideoModel);
                        console.log('count : ', count, 'allContestVideo.length : ', allContestVideo.length);
                        total = Math.ceil(count / allContestVideo.length);
                        console.log(total);
                    }
                    this.res.status(200).json({
                        status: 200,
                        message: 'success',
                        data: {
                            message: 'video list',
                            videos: await this.formatVideos(
                                allContestVideo,
                                uid
                            ),
                        },
                        pagination: {
                            total,
                            page: parseInt(page),
                            next:
                                parseInt(page) < total
                                    ? parseInt(page) + 1
                                    : null,
                            prev:
                                parseInt(page) <= total && parseInt(page) !== 1
                                    ? parseInt(page) - 1
                                    : null,
                        },
                    });
                    break;

                default:
                    // get non contest videos
                    user = await UserModel.findById(uid);
                    const blocked = user ? user.blockedUsers : [];
                    console.log('blocked', blocked);
                    console.log('privateUserProfile', privateUserProfile);
                    query = {
                        contest: 2,
                        _id: {
                            $nin: [...videosHidden]
                        },
                        uploader: {
                            $nin: [
                                ...blocked,
                                ...privateUserProfile,
                                ...userWhoBlockedMe,
                            ],
                        },
                    };
                    console.log('ggg', query);
                    count = await VideoModel.countDocuments(query);
                    let allvideos;
                    allvideos = await videosFlow(query, limit, page, VideoModel);
                    total = Math.ceil(count / allvideos.length);
                    // if ((count - (limit * (page - 1))) > limit) {
                        
                    // } else {
                    //     allvideos = await VideoModel.find(query)
                    //         .skip(parseInt(limit) * (parseInt(page) - 1))
                    //     // .skip(random)
                    //         .limit(parseInt(limit))

                    //         .sort([[sort, -1]]);
                    //     total = Math.ceil(count / limit);
                    // }
                    // var random = Math.floor(Math.random() * count);
                    // const allvideos: any = await VideoModel.find(query)
                    //     .skip(parseInt(limit) * (parseInt(page) - 1))
                    //     .limit(parseInt(limit))

                    //     .sort([[sort, -1]]);

                    // const allvideos: any = VideoModel.aggregate([
                    //     // filter the results
                    //     { $sample: { size: limit } },
                    //     // { $skip: parseInt(limit) * (parseInt(page) - 1) }, // You want to get 5 docs
                    // ]);

                    // db.users.aggregate(
                    //     [ { $sample: { size: 3 } } ]
                    //  )
                    // console.log('allvideos', allvideos);

                    
                    this.res.status(200).json({
                        status: 200,
                        message: 'success',
                        data: {
                            message: 'video list',
                            videos: await this.formatVideos(allvideos, uid),
                        },
                        pagination: {
                            total,
                            page: parseInt(page),
                            next:
                                parseInt(page) < total
                                    ? parseInt(page) + 1
                                    : null,
                            prev:
                                parseInt(page) <= total && parseInt(page) !== 1
                                    ? parseInt(page) - 1
                                    : null,
                        },
                    });
                    break;
            }
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    videos: [],
                },
                pagination: {},
            });
        }
    };

    uploadVideo = async () => {
        if (this.req.files || Object.keys(this.req.files).length === 0) {
            try {
                const video = this.req.files.video;
                console.log('video', video);
                const thumbnail = this.req.files.thumbnail;
                console.log('thumbnail', thumbnail);
                const postedById = this.req.uid;
                const postedByName = this.req.user.username;
                const path = 'videos/' + uuidv4() + '/';
                const videopath = path + uuidv4() + video.name;
                const thumbnailPath =
                    path + 'thumbnail/' + uuidv4() + thumbnail.name;
                const uploader: any = await UserModel.findById(this.req.uid);

                const videoMimeType =
                    video && video.mimetype ? video.mimetype : 'video/mpeg';

                const thumbnailMimeType =
                    thumbnail && thumbnail.mimetype
                        ? thumbnail.mimetype
                        : 'image/jpeg';
                const videoUrl = s3.getBucketUrl() + '/' + videopath;
                const thumbnailUrl = s3.getBucketUrl() + '/' + thumbnailPath;

                await s3.uploadObject({
                    Bucket: this.bucket,
                    Key: videopath,
                    Body: video.data,
                    ACL: 'public-read',
                    ContentType: videoMimeType,
                    ContentDisposition: 'inline',
                });

                await s3.uploadObject({
                    Bucket: this.bucket,
                    Key: thumbnailPath,
                    Body: thumbnail.data,
                    ACL: 'public-read',
                    ContentType: thumbnailMimeType,
                    ContentDisposition: 'inline',
                });

                // update in db

                const videodb = await new VideoModel({
                    originalVideo: videoUrl,
                    postedById,
                    postedByName,
                    profileImage: this.req.user.image,
                    thumbnails: thumbnailUrl,
                    description: this.req.body.description,
                    songName: this.req.body.songName,
                    tags: this.req.body.tags,
                    category: this.req.body.category,
                    uploader,
                }).save();

                uploader.uploadedVideos.push(videodb);

                await uploader.save();

                this.res.status(200).json({
                    status: 200,
                    message: 'success',
                    data: {
                        message: 'Video uploaded succesfully',
                    },
                });
            } catch (error) {
                logger.error(error);
                console.log(error);
                this.res.status(400).json({
                    status: 400,
                    message: 'error',
                    data: {
                        message: error.message,
                    },
                });
            }

            // upload file to space
        } else {
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: 'please upload video with name video',
                },
            });
        }
    };

    delete = async () => {
        try {
            const { id } = this.req.params;
            const video: any = await VideoModel.findById(id);
            // comments on the video
            const comments: any = await CommentModel.find({
                video: video._id,
            });

            // delete all comments on video
            for (const comment of comments) {
                await comment.deleteOne();
            }

            const users: any = await UserModel.find({
                $or: [
                    {
                        uploadedVideos: video._id,
                    },
                    {
                        likedVideos: video._id,
                    },
                    {
                        dislikedVideos: video._id,
                    },
                    {
                        sharedVideos: video._id,
                    },
                    {
                        viewedVideos: video._id,
                    },
                ],
            });

            if (users.length >= 1) {
                for (const user of users) {
                    console.log(user);
                    await user.uploadedVideos.pull(video);
                    await user.likedVideos.pull(video);
                    await user.dislikedVideos.pull(video);
                    await user.sharedVideos.pull(video);
                    await user.dislikedVideos.pull(video);
                    await user.viewedVideos.pull(video);
                }
            }

            await video.deleteOne();
            this.res.status(200).json({
                status: 200,
                message: 'success',
                data: {
                    message: 'Video deleted successfully',
                },
            });
        } catch (error) {
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                },
            });
        }
    };

    getVideo = async () => {
        try {
            const uid = this.req.uid;
            const { id } = this.req.params;
            const video: any = await VideoModel.findById(id);
            if (!video) throw new Error('Please provide valid id');
            // // now need to make sure same video response format
            const userLiked = video.userLiked;
            if (userLiked.includes(uid)) {
                video.myLikeStatus = true;
            } else {
                video.myLikeStatus = false;
            }
            const uploader: any = await UserModel.findById(video.uploader);
            const uploaderFollwers: string[] = uploader
                ? uploader.follwersUsers
                : [];

            if (uploaderFollwers.length >= 1) {
                if (uploaderFollwers.includes(uid)) {
                    video.myfollwStatus = true;
                    video.followuser = true;
                } else {
                    video.myfollwStatus = false;
                    video.followuser = false;
                }
            } else {
                video.myfollwStatus = false;
                video.followuser = false;
            }

            video.postedByName = uploader.username;
            video.profileImage = uploader.image;
            video.isUserVerified = uploader.isUserVerified;
            this.res.status(200).json({
                status: 200,
                message: 'success',
                data: {
                    message: 'video info',
                    video,
                },
            });
        } catch (error) {
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    video: {},
                },
            });
        }
    };
}

export default VideoController;
