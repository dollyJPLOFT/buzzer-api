import { Request, Response, NextFunction } from 'express';
import ReportProblemModel from '../../Models/ReportProblem';
/**
 * Report Problem Controller Class
 * @author Dolly Garg <dollygarg137@gmail.com>
 */
class ReportProblemController {
    req: any;
    res: Response;
    next: NextFunction;

    /**
     * Constructor
     * @param req express.Request
     * @param res express.Response
     * @param next express.NextFunction
     */

    constructor(req: Request, res: Response, next: NextFunction) {
        this.req = req;
        this.res = res;
        this.next = next;
    }

    report = async () => {
        try {
            const userID = this.req.uid;
            const {description} = this.req.body; // destructure
            if (!description)
                throw new Error('Please provide description in request body');
            // save problem
            const doc = await new ReportProblemModel({
                userID,
                description
            }).save();
            this.res.status(200).json({
                status: 200,
                message: 'success',
                data: {
                    message: 'Report is sent',
                    doc,
                },
            });
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    transaction: {},
                },
            });
        }
    };
}

export default ReportProblemController;