import { Request, Response, NextFunction } from 'express';
import VideoModel from '../../Models/Video';
import UserModel from '../../Models/User';
import ContestModel from '../../Models/Contest';
import ContestVote from '../../Models/ContestVote';

/**
 * @author Dolly Garg <dollygarg137@gmail.com>
 */

class ContestController {
    req: any;
    res: Response;
    next: NextFunction;
    bucket: any;

    /**
     * Constructor
     * @param req express.Request
     * @param res  express.Response
     * @param next   express.NextFunction
     */

    constructor(req: Request, res: Response, next: NextFunction) {
        this.req = req;
        this.res = res;
        this.next = next;
        this.bucket = process.env.DIGITAL_OCEAN_SPACE_NAME
            ? process.env.DIGITAL_OCEAN_SPACE_NAME
            : 'buzzer';
    }

    formatContest = async (contest, uid) => {
        console.log(contest);
        let data: any = [];
        let videos = contest.videos;
        let participated = false;
        if (videos.length >= 1) {
            for (let video of videos) {
                delete video._id;
                video = { ...video, ...video.videoId };
                // console.log('formated video : ', video);
                video.videoId = video._id;
                video.likeCount = video.userLiked.length;
                const userLiked = video.userLiked;
                if (userLiked.includes(uid)) {
                    video.myLikeStatus = true;
                } else {
                    video.myLikeStatus = false;
                }
                const uploader: any = await UserModel.findById(video.uploader);
                const uploaderFollwers: string[] = uploader
                    ? uploader.follwersUsers
                    : [];

                if (uploaderFollwers.length >= 1) {
                    if (uploaderFollwers.includes(uid)) {
                        video.myfollwStatus = true;
                        video.followuser = true;
                    } else {
                        video.myfollwStatus = false;
                        video.followuser = false;
                    }
                } else {
                    video.myfollwStatus = false;
                    video.followuser = false;
                }

                video.postedByName = uploader.username;
                video.profileImage = uploader.image;
                video.isUserVerified = uploader.isUserVerified;

                if (!participated) {
                    participated = video.votingUser
                        .map(user => user.toString())
                        .includes(uid);
                }

                delete video.votingUser;
                data.push(video);
            }
        }
        contest.participated = participated;
        contest.eliminated = contest.eliminatedUser
            .map(user => user.toString())
            .includes(uid);
        delete contest.eliminatedUser;
        contest.videos = data;
        return contest;
    };

    formatUser = async (users, uid) => {
        console.log(typeof uid);
        for (const user of users) {
            user.isFollow = user.follwersUsers
                .map(userId => userId.toString())
                .includes(uid);
        }
        return users;
    };

    formatWinner = async list => {
        let data = [];
        console.log(list);
        for (const winner of list.videos) {
            // user.isFollow = user.follwersUsers
            // .map(user => user.toString())
            // .includes(uid);
            winner.videoId.viewCount = winner.videoId.userViewed.length;
            delete winner.videoId.userViewed;
            data.push({
                votingCount: winner.votingUser.length,
                position: winner.position,
                group: winner.group,
                user: winner.uploader,
                video: winner.videoId,
            });
        }
        data.sort(function (a, b) {
            return b.votingCount - a.votingCount;
        });
        return data;
    };

    getContest = async () => {
        try {
            let query;
            const uid = this.req.uid;
            query = {
                winnerDeclared: false
            };
            // getting contest in which winner is not declared and is created recently.
            const contest = await ContestModel.findOne(query).sort([['createdAt', -1]])
                .populate('videos.videoId').lean();
            // console.log(contest.videos);
            if (contest) {
                this.res.status(200).json({
                    status: 200,
                    message: 'success',
                    data: {
                        message: 'contest data',
                        contest: await this.formatContest(contest, uid),
                    },
                });
            } else {
                this.res.status(200).json({
                    status: 408,
                    message: 'success',
                    data: {
                        message: 'There was no contest (Minimum 8 videos Required)',
                        contest: {}
                    },
                });
            }
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    contest: {},
                },
                pagination: {},
            });
        }
    };

    voteForVideo = async () => {
        try {
            const { contestId, videoId } = this.req.body; // destructure
            if (!contestId) throw new Error('Please provide contest id');
            if (!videoId) throw new Error('Please provide video id');
            let contest: any = await ContestModel.findOne({ _id: contestId });
            if (!contest) throw new Error('Invalid contest id');
            // console.log(" contest : ", contest);
            if (contest.winnerDeclared === true)
                throw new Error(
                    'Winner declared for this contest, you can not vote for this video.'
                );
            // updated model
            const contestModelDoc = await ContestModel.updateOne(
                { _id: contestId, 'videos.videoId': videoId },
                { $addToSet: { "videos.$.votingUser": this.req.uid }}
            );
            console.log('contestModelDoc : ', contestModelDoc);

            let history = await ContestVote.findOne(
                {$and: [
                    {contestId: contestId}, 
                    {voterId: this.req.uid}
                ]});
            
            // if no histroy then save history
            if (!history) {
                await new ContestVote({
                    videoId: videoId,
                    contestId: contestId,
                    voterId: this.req.uid,
                }).save();
            }

            this.res.status(200).json({
                status: 200,
                message: 'success',
                data: {
                    message: 'Vote added successfully, please check the leadership board for the results',
                    vote: {},
                },
            });
        } catch (error) {
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    vote: {},
                },
            });
        }
    };

    getParticipateUserCount = async () => {
        try {
            let query;
            const uid = this.req.uid;
            const { contestId } = this.req.query; // destructure
            query = {
                _id: contestId,
            };
            const contest: any = await ContestModel.findOne(query, {}).lean();
            if (contest) {
                let count = 0;
                let participated = false;
                for (let video of contest.videos) {
                    count += video.votingUser.length; // total participtants are the voting users length
                    if (!participated) {
                        participated = video.votingUser
                            .map(user => user.toString())
                            .includes(uid);
                    }
                }
                this.res.status(200).json({
                    status: 200,
                    message: 'success',
                    data: {
                        message: 'contest user count',
                        contest: {
                            userCount: contest.participants,
                            eliminated: contest.eliminatedUser
                                .map(user => user.toString())
                                .includes(uid),
                            participated: participated,
                        },
                    },
                });
            } else {
                throw new Error('No contest availabe for today');
            }
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    contest: {},
                },
                pagination: {},
            });
        }
    };

    participate = async () => {
        try {
            let query;
            const uid = this.req.uid;
            const { contestId } = this.req.query; // destructure
            query = {
                _id: contestId,
            };
            const contest = await ContestModel.findOne(query);
            if (contest) {
                contest.participants = +contest.participants + 1; // update number of participants.
                await contest.save(); // save the contest
                this.res.status(200).json({
                    status: 200,
                    message: 'success',
                    data: {
                        message: 'contest user count',
                        contest: {
                            userCount: contest.participants
                        },
                    },
                });
            } else {
                throw new Error('No contest availabe');
            }
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    contest: {},
                },
                pagination: {},
            });
        }
    };

    getPstDate = async () => {
        // send time as per the locals
        try {
            this.res.status(200).json({
                status: 200,
                message: 'success',
                data: {
                    message: 'Pst time',
                    pstTime: +new Date(
                        new Date().toLocaleString('en-US', {
                            timeZone: 'America/Los_Angeles',
                        })
                    ),
                },
            });
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    pstTime: new Date(),
                },
                pagination: {},
            });
        }
    };

    eliminate = async () => {
        try {
            const { contestId } = this.req.body; // destructure
            if (!contestId) throw new Error('Please provide contest id');
            let contest: any = await ContestModel.findOne({ _id: contestId });
            if (!contest) throw new Error('Invalid contest id');

            // updated model
            await ContestModel.updateOne(
                { _id: contestId },
                {
                    $addToSet: { eliminatedUser: this.req.uid },
                }
            );

            this.res.status(200).json({
                status: 200,
                message: 'success',
                data: {
                    message: 'Eliminated user fron contest',
                    vote: {},
                },
            });
        } catch (error) {
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    vote: {},
                },
            });
        }
    };

    winnerList = async () => {
        try {
            let query;
            query = {
                winnerDeclared: true,
            };
            const count = await ContestModel.countDocuments(query);
            const winner: any = await ContestModel.findOne(query)
                .select({
                    _id: 1,
                    winnerUserId: 1,
                    winnerVideoId: 1,
                    videos: 1,
                })
                .populate([
                    {
                        path: 'videos.uploader',
                        model: UserModel,
                        select: {
                            _id: 1,
                            firstName: 1,
                            lastName: 1,
                            middleName: 1,
                            name: 1,
                            image: 1,
                            followers: 1,
                            following: 1,
                            likes: 1,
                            username: 1,
                            email: 1,
                            earning: 1,
                        },
                    },
                    {
                        path: 'videos.videoId',
                        model: VideoModel,
                        select: {
                            mentions: 0,
                            category: 0,
                            userLiked: 0,
                            userDisliked: 0,
                            userFollowed: 0,
                            userUnfollwed: 0,
                            userBlocked: 0,
                            userShared: 0,
                            //userViewed: 0,
                            comments: 0,
                        },
                    },
                ])
                .sort([['_id', -1]])
                .lean();
            if (winner) {
                this.res.status(200).json({
                    status: 200,
                    message: 'success',
                    data: {
                        message: 'Winner list',
                        winner: await this.formatWinner(winner),
                    },
                    pagination: {
                        total: count,
                        page: 1,
                        next: null,
                        prev: null,
                    },
                });
            } else {
                throw new Error('No winner list');
            }
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    winner: {},
                },
                pagination: {},
            });
        }
    };

    topCreator = async () => {
        try {
            // creator of the the most liked video.
            let contest: any = await ContestModel.find({winnerDeclared: true})
                .sort([['updatedAt', -1]])
                .limit(1)
                .select('videos')
                .populate([{
                    path: 'videos.uploader',
                    select:
                        'id username earning firstName middleName lastName name image isUserVerified isFollow follwersUsers isEmailVerified isMobileVerified',
                },
                {
                    path: 'videos.videoId',
                    select: 'userLiked'
                }]).lean();
            if (contest && contest.length) {
                const contestVideos = contest[0].videos;
                const creator = contestVideos.map(video => {
                    video.uploader = {...video.uploader};
                    video.uploader['likes'] = video.videoId.userLiked.length; // update likes for every video
                    return video.uploader;
                });
                this.res.status(200).json({
                    status: 200,
                    message: 'success',
                    data: {
                        message: 'Creator list',
                        creator: await this.formatUser(creator, this.req.uid),
                    },
                });
            } else {
                throw new Error('No top creator list');
            }
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    creator: {},
                },
            });
        }
    };

    topViewer = async () => {
        try {
            // get recent contest
            const contest: any = await ContestModel.findOne({winnerDeclared: true})
                .sort([['updatedAt', -1]]).lean();
            if (!contest) {
                throw new Error('No contest has been made');
            }
            // query for the top voters sorted in ascending order as per their voting time.
            const query = [
                {
                    $match: {
                        contestId: contest._id
                    }
                },
                {
                    $sort: {
                        createdAt: 1
                    }
                },
                {
                    $limit: 8
                }, 
                {
                    $lookup: {
                        from: 'users',
                        localField: 'voterId',
                        foreignField: '_id',
                        as: 'voterId'
                    }
                }, 
                {
                    $unwind: {
                        path: '$voterId'
                    }
                }
            ];
            let topViewers = await ContestVote.aggregate(query);
            if (topViewers.length) {
                topViewers = topViewers.map((viewers) => {
                    return viewers.voterId;
                });
                this.res.status(200).json({
                    status: 200,
                    message: 'success',
                    data: {
                        message: 'Viewer list',
                        viewer: await this.formatUser(topViewers, this.req.uid),
                    },
                });
            } else {
                throw new Error('No top viewer');
            }
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    viewer: {},
                },
            });
        }
    };
}

export default ContestController;
