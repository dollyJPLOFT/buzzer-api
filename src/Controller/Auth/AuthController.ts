import { Request, Response, NextFunction } from 'express';
import { sign } from 'jsonwebtoken';
import UserModel from '../../Models/User';
import OtpModel from '../../Models/Otp';
import SMSAPI from '../Sms/Sms';
import logger from '../../utilis/logger';
import { compareSync, hashSync } from 'bcryptjs';
import Mail from '../../Helper/Mail';
import VideoPreferenceModel from '../../Models/VideosPreference';
import SNSModel from '../../Models/SNS';
import VideoModel from '../../Models/Video';

import {
    constructName,
    generateUserName,
    generateOTP,
} from '../../Helper/auth';
/**
 *  Auth Controller Class
 *  @author Jai Sharma <jaiprakash.sharma44@gmail.com>
 */
class AuthController {
    req: any;
    res: Response;
    next: NextFunction;

    /**
     * Constructor
     * @param req express.Request
     * @param res  express.Response
     * @param next   express.NextFunction
     */

    constructor(req: Request, res: Response, next: NextFunction) {
        this.req = req;
        this.res = res;
        this.next = next;
    }
    /**
     * 
     * @param mobile number
     * @returns boolean
     */
    isValidMobile = mobile => {
        //check for special charters except +;
        const pattern = /([^\w+])/g;
        const checkforChar = /[A-Z,a-z]/;
        const isSpecialChar = pattern.test(mobile);
        const isChar = checkforChar.test(mobile);
        // if no characters except + and only digits then it is valid mobile number
        if (!isSpecialChar && !isChar) {
            return true;
        } else {
            return false;
        }
    };

    /**
     * 
     * @param uid unique id
     * @param scope object
     * @returns token
     */
    generateToken = (uid: string, scope: string) => {
        const appSecret: string = process.env.JWT_SECRET;
        // using json web token to generate the token
        const token = sign(
            {
                uid,
                scope,
            },
            appSecret
        );

        return token;
    };

    sendOTP = async () => {
        const mobile = this.req.body.mobile;
        console.log('type of', typeof mobile);
        console.log('body', this.req.body);
        if (mobile) {
            try {
                const isValidMobile = this.isValidMobile(mobile);
                if (!isValidMobile)
                    throw new Error('Please provide valid mobile number');
                // generate otp for mobile verification
                const otp = generateOTP();
                // send otp to valid mobile number
                const response = await SMSAPI.sendOtp(mobile, otp);
                // storing the session id returned by sms api for later steps like while verifying opt entered by user.
                const sessionID = response.data.Details;
                const data = {
                    otp,
                    mobile,
                    sessionID,
                };

                // get all other doc
                const docs = await OtpModel.find({
                    mobile,
                });

                // deleting all other previous doc with mobile given
                for (const doc of docs) {
                    await doc.deleteOne();
                }

                // and saving the new one in otps collection
                await new OtpModel(data).save();
                this.res.status(200).json({
                    status: 200,
                    message: 'success',
                    data: {
                        message: 'OTP sent successfully',
                        otp,
                        mobile,
                    },
                });
            } catch (error) {
                console.log(error);
                console.log(
                    error.response ? error.response.data.Details : error.message
                );
                this.res.status(400).json({
                    status: 400,
                    message: 'error',
                    data: {
                        message: error.response
                            ? error.response.data.Details
                            : error.message,
                        otp: '',
                        mobile: '',
                    },
                });
                logger.error(error);
            }
        } else {
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: 'Please provide mobile no with country code',
                    otp: '',
                    mobile: '',
                },
            });
        }
    };

    verifyOtp = async () => {
        try {
            // extracting mobile anf otp from request body
            const { mobile, otp } = this.req.body;
            if (!mobile || !otp)
                throw new Error('Please provide otp and mobile no');
            // verify otp and mobile 
            const otpDoc = await OtpModel.findOne({
                otp,
                mobile,
            });
            if (!otpDoc) throw new Error('invalid otp');
            let user: any;
            // check if user exist
            user = await UserModel.findOne({
                mobile,
            });
            if (user) {
                // update mobile
                user.mobile = mobile;
                await user.save();
            } else {
                // create user
                user = new UserModel({
                    mobile,
                    username: generateUserName(),
                });
                const videoPreference = new VideoPreferenceModel({
                    user,
                });
                user.videoPreference = videoPreference;
                await user.save();
                await videoPreference.save();
            }
            
            // delete otp doc.
            await otpDoc.deleteOne();
            // generate token
            const token = this.generateToken(user._id, user.role);
            // send response
            this.res.status(200).json({
                status: 200,
                message: 'success',
                data: {
                    message: 'login successful',
                    token,
                },
            });
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: "Couldn't verify otp. Please try again",
                    token: '',
                },
            });
            logger.error(error);
        }
    };

    /**
     *  Login Method
     */
    login = async () => {
        try {
            const { email, password } = this.req.body;
            if (!email || !password)
                throw new Error('Please provide Email and Password');
            // check user
            const user = await UserModel.findOne({
                email,
            });
            if (!user) throw new Error('Please provide valid email address');
            // comparing password provided with the hex password stored in database
            const isPasswordAMatch = compareSync(password, user.password);
            if (!isPasswordAMatch)
                throw new Error('Please check your password');
            // get token
            const token = this.generateToken(user._id, user.role);
            let userData = await this.profile(user.id);
            this.res.status(200).json({
                status: 200,
                message: 'success',
                data: {
                    message: 'Login successfull',
                    token,
                    info: userData,
                },
            });
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: 'Invalid login details',
                    token: '',
                },
            });
        }
    };

    signupWithEmail = async () => {
        try {
            let { email, name, username, password, mobile } = this.req.body;

            console.log('this.req body', this.req.body);
            username = generateUserName(username);
            const isValidMobile = this.isValidMobile(mobile);
            if (mobile && !isValidMobile)
                throw new Error('Please provide valid mobile number');
            if (!email || !password)
                throw new Error('Please provide email and password');
            // check email exsist
            const isEmailExsist = await UserModel.findOne({ email });
            if (isEmailExsist) {
                // throw new Error('This email already exist');
                return this.res.status(400).json({
                    status: 409,
                    message: 'error',
                    data: {
                        message: 'This email already exist',
                        user: '',
                    },
                });
            }
            if (mobile) {
                const isMobileExsist = await UserModel.findOne({ mobile });
                if (isMobileExsist)
                    throw new Error('This mobile number already exist');
            }
            if (username) {
                const isUsernameExsist = await UserModel.findOne({
                    username,
                });
                if (isUsernameExsist)
                    throw new Error('This username already exist');
            }

            let data: any = {};
            if (name) {
                const { firstName, middleName, lastName } = constructName(name);
                data.firstName = firstName;
                data.middleName = middleName;
                data.lastName = lastName;
            }
            // set username
            // if (username && username.length < 6)
            //     throw new Error('username should be 6 chracter long');

            data.username = generateUserName(username);
            // hex conversion of password / encryption pf password
            data.password = hashSync(password.toString());
            const user = new UserModel({
                ...this.req.body,
                ...data,
            });
            const videoPreference = new VideoPreferenceModel({
                user,
            });
            // user.videoPreference = videoPreference;

            await user.save();
            await videoPreference.save();
            this.res.status(200).json({
                status: 200,
                message: 'success',
                data: {
                    message: 'user registered successfully',
                    user,
                },
            });
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    //  message:
                    // 'Please check your provided details. And try again',
                    message: error.message,
                    user: '',
                },
            });
            logger.error(error);
        }
    };

    /**
     *  Login Method
     */
    logout = async () => {
        try {
            const uid = this.req.uid;
            // on log out reseting arn end points for device so that no notifications would be sent to those users.
            const sns = await SNSModel.findOne({ user: uid });
            // null them
            sns.deviceToken = '';
            sns.deviceID = '';
            sns.awsArnEndpoint = '';
            await sns.save();
            this.res.status(200).json({
                status: 200,
                message: 'success',
                data: {
                    message: 'logout succesfull',
                    sns,
                },
            });
        } catch (error) {}
    };

    /**
     * Password Forgot Method
     */
    passwordForgot = async () => {
        try {
            const mobile = this.req.body.mobile;
            const email = this.req.body.email;
            if (!email && !mobile)
                throw new Error('Please provide either email or mobile');
            if (mobile) {
                const user = await UserModel.findOne({
                    mobile,
                });
                if (!user) throw new Error('No user found with this mobile');
                const otp = generateOTP();
                const response = await SMSAPI.sendOtp(mobile, otp);
                const sessionID = response.data.Details;
                const data = {
                    otp,
                    mobile,
                    sessionID,
                };
                await new OtpModel(data).save();
                this.res.status(200).json({
                    status: 200,
                    message: 'success',
                    data: {
                        message: 'otp sent to mobile',
                    },
                });
            } else {
                const user = await UserModel.findOne({
                    email,
                });
                if (!user) throw new Error('No user found with this email');
                const otp = generateOTP();
                const text = `Your OTP is ${otp}`;
                await Mail.send(email, 'Recover Your Buzzer Account', text);
                await new OtpModel({
                    otp,
                    email,
                }).save();
                this.res.status(200).json({
                    status: 200,
                    message: 'success',
                    data: {
                        message: 'otp sent to email',
                    },
                });
            }
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    // message: 'Please try again',
                    message: error.message,
                },
            });
        }
    };

    /**
     * Password Reset Method
     */
    passwordReset = async () => {
        try {
            const { otp, mobile, password, email } = this.req.body;
            if (!otp && !password && !email)
                throw new Error('Please provide otp, and  password');
            // checking for any combination of otp and mobile or otp and email
            const otpDoc = await OtpModel.findOne({
                $or: [
                    {
                        otp,
                        mobile,
                    },
                    {
                        otp,
                        email,
                    },
                ],
            });
            if (!otpDoc) throw new Error('Invalid OTP');

            if (mobile) {
                // update user
                await UserModel.findOneAndUpdate(
                    {
                        mobile,
                    },
                    {
                        password: hashSync(password.toString()),
                    }
                );
            } else {
                // update user
                await UserModel.findOneAndUpdate(
                    {
                        email,
                    },
                    {
                        password: hashSync(password.toString()),
                    }
                );
            }

            this.res.status(200).json({
                status: 200,
                message: 'success',
                data: {
                    message: 'password changed successfully',
                },
            });
        } catch (error) {
            console.log(error);
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    // message: 'please try again',
                    message: error.message,
                },
            });
        }
    };

    /**
     * 
     * @param id - user id
     * @returns 
     */
    profile = async id => {
        try {
            const q = this.req.query.q;
            const uid = id;
            const limit =
                this.req.query.limit && parseInt(this.req.query.limit) !== 0
                    ? this.req.query.limit
                    : 2;
            const page =
                this.req.query.page && parseInt(this.req.query.page) !== 0
                    ? this.req.query.page
                    : 1;

            let user;
            let count;
            let total;
            let query;
            let me;
            let response;

            switch (q) {
                case 'followers':
                    // query to find the users following the logined user with paginations
                    user = await UserModel.findById(uid).populate({
                        path: 'follwersUsers',
                        select:
                            'id  username  firstName middleName lastName name image isUserVerified isFollow',
                        options: {
                            skip: parseInt(limit) * (parseInt(page) - 1),
                            limit: parseInt(limit),
                        },
                    });

                    
                    me = await UserModel.findById(uid);
                    count = me.follwersUsers.length;
                    const followers = user ? user.follwersUsers : [];
                    total = Math.ceil(count / limit);
                    // getting the list of all followers and checking if the logined person is following the follower or not.
                    for (const follower of followers) {
                        const other = await UserModel.findById(follower._id);
                        // console.log('other user', other);
                        const otherUserFollwers: string[] = other
                            ? other.follwersUsers
                            : [];

                        if (otherUserFollwers.length >= 1) {
                            if (otherUserFollwers.includes(uid)) {
                                follower.isFollow = true;
                            } else {
                                follower.isFollow = false;
                            }
                        } else {
                            follower.isFollow = false;
                        }
                    }
                    response = followers;
                    break;

                case 'following':
                    user = await UserModel.findById(uid).populate({
                        path: 'follwingUsers',
                        select:
                            'id  username  firstName middleName lastName name image isUserVerified isFollow',
                        options: {
                            skip: parseInt(limit) * (parseInt(page) - 1),
                            limit: parseInt(limit),
                        },
                    });

                    const following = user ? user.follwingUsers : [];
                    me = await UserModel.findById(uid);
                    count = me.follwingUsers.length;
                    total = Math.ceil(count / limit);
                    for (const followingUser of following) {
                        const other = await UserModel.findById(
                            followingUser._id
                        );
                        //  console.log('other user', other);
                        const otherUserFollwers: string[] = other
                            ? other.follwersUsers
                            : [];

                        if (otherUserFollwers.length >= 1) {
                            if (otherUserFollwers.includes(uid)) {
                                followingUser.isFollow = true;
                            } else {
                                followingUser.isFollow = false;
                            }
                        } else {
                            followingUser.isFollow = false;
                        }
                    }
                    response = following;
                    break;
                case 'blocked':
                    // getting the list of all blocked Users
                    user = await UserModel.findById(uid).populate(
                        'blockedUsers',
                        'id  username  firstName middleName lastName name image isUserVerified isFollow isBlocked'
                    );
                    
                    const blocked = user ? user.blockedUsers : [];
                    // setting blocked to true and following to false
                    for (const u of blocked) {
                        u.isBlocked = true;
                        u.isFollow = false;
                        await u.save();
                    }
                    count = blocked.length;
                    total = Math.ceil(count / limit);
                    // console.log('total', total);
                    response = blocked;
                    break;

                case 'uploaded':
                    // querying for videos uploaded by the logined user.
                    query = {
                        uploader: uid,
                    };
                    const uploadedVideos = await VideoModel.find(query)
                        .skip(parseInt(limit) * (parseInt(page) - 1))
                        .limit(parseInt(limit))
                        .sort([['createdAt', -1]]);
                    // adding pagination.
                    count = await VideoModel.countDocuments(query);
                    total = Math.ceil(count / limit);
                    // formatting videos
                    response = await this.formatVideos(uploadedVideos, uid);
                    break;

                case 'liked':
                    // user
                    //blocked
                    // nin uploder blocked

                    user = await UserModel.findById(uid);
                    const blockedUsers = user.blockedUsers;
                    // querying for liked videos where uploader should not be a blocked user.
                    query = {
                        userLiked: uid,
                        uploader: {
                            $nin: [...blockedUsers],
                        },
                    };
                    const likedVideos = await VideoModel.find(query)
                        .skip(parseInt(limit) * (parseInt(page) - 1))
                        .limit(parseInt(limit))
                        .sort([['createdAt', -1]]);
                    count = await VideoModel.countDocuments(query);
                    total = Math.ceil(count / limit);
                    response = await this.formatVideos(likedVideos, uid);
                    break;
                case 'shared':
                    // querying for videos shared by logined user.
                    query = {
                        userShared: uid,
                    };
                    const sharedVideos = await VideoModel.find(query)
                        .skip(parseInt(limit) * (parseInt(page) - 1))
                        .limit(parseInt(limit))
                        .sort([['createdAt', -1]]);
                    count = await VideoModel.countDocuments(query);
                    total = Math.ceil(count / limit);
                    response = this.formatVideos(sharedVideos, uid);
                    break;

                case 'viewed':
                    // querying for videos videos by logined user.
                    query = {
                        userViewed: uid,
                    };

                    const viewedVideos = await VideoModel.find(query)
                        .skip(parseInt(limit) * (parseInt(page) - 1))
                        .limit(parseInt(limit))
                        .sort([['createdAt', -1]]);
                    count = await VideoModel.countDocuments(query);
                    total = Math.ceil(count / limit);
                    response = await this.formatVideos(viewedVideos, uid);
                    break;

                default:
                    // fetching videos of logined user.
                    user = await UserModel.findById(uid).select('-password ');
                    const myVideos = await VideoModel.find({
                        uploader: uid,
                    });
                    let likes = 0;
                    myVideos.forEach(video => {
                        likes += video.userLiked.length;
                    });
                    user.likes = likes;
                    user.save();
                    response = user;
                    break;
            }
            return response;
        } catch (error) {
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    data: {},
                },
            });
        }
    };

    formatVideos = async (videos, uid) => {
        let data: any = [];
        // formating the videos like setting my like status, myfollwStatus and followeruser for every video.
        if (videos.length >= 1) {
            for (const video of videos) {
                const userLiked = video.userLiked;
                if (userLiked.includes(uid)) {
                    video.myLikeStatus = true;
                } else {
                    video.myLikeStatus = false;
                }
                //   console.log('video', video);
                const uploader: any = await UserModel.findById(video.uploader);
                const uploaderFollwers: string[] = uploader
                    ? uploader.follwersUsers
                    : [];

                if (uploaderFollwers.length >= 1) {
                    if (uploaderFollwers.includes(uid)) {
                        video.myfollwStatus = true;
                        video.followuser = true;
                    } else {
                        video.myfollwStatus = false;
                        video.followuser = false;
                    }
                } else {
                    video.myfollwStatus = false;
                    video.followuser = false;
                }

                video.postedByName = uploader.username;
                video.profileImage = uploader.image;
                video.isUserVerified = uploader.isUserVerified;

                data.push(video);
            }
        }

        return data;
    };
}

export default AuthController;
