import { Request, Response, NextFunction } from 'express';
import InterestModel from '../../Models/Interest';
import AWS, { S3 } from 'aws-sdk';
import { v4 as uuidv4 } from 'uuid';
class InterestController {
    req: any;
    res: Response;
    next: NextFunction;
    bucket: string;
    region: string;
    s3: S3;
    endpoint: string;
    cdn: string;

    /**
     * Constructor
     * @param req express.Request
     * @param res  express.Response
     * @param next   express.NextFunction
     */

    constructor(req: Request, res: Response, next: NextFunction) {
        this.req = req;
        this.res = res;
        this.next = next;
        // configuration for uploading image to s3 bucket of AWS
        this.bucket = process.env.AWS_S3_BUCKET_NAME;
        this.region = process.env.AWS_S3_REGION;
        this.endpoint = process.env.AWS_S3_ENDPOINT;
        this.s3 = new AWS.S3({
            accessKeyId: process.env.AWS_S3_ACCESS_KEY_ID,
            secretAccessKey: process.env.AWS_S3_SECRET_KEY,
        });
        this.cdn = process.env.AWS_CLOUDFRONT_DOMAIN;
    }

    getCdnUrl = path => {
        return this.cdn + '/' + path;
    };

    uploadObject = async (params: S3.PutObjectRequest) => {
        return await this.s3.putObject(params).promise();
    };

    add = async () => {
        try {
            const { name } = this.req.body;
            if (!name) throw new Error('Please provide interest name');
            if (this.req.files && this.req.files.image) {
                const image = this.req.files.image; // using multer to extract files
                const path = 'interest/image/' + uuidv4() + '/'; // contruct a path for file to save at
                const imagepath = path + image.name; 
                const imageMimeType =
                    image && image.mimetype ? image.mimetype : 'image/png'; // defining mime type for file
                const imageUrl = this.getCdnUrl(imagepath); // get constrcuted cloud domain url 
                // finally upload file
                await this.uploadObject({
                    Bucket: this.bucket,
                    Key: imagepath,
                    Body: image.data,
                    ACL: 'public-read',
                    ContentType: imageMimeType,
                    ContentDisposition: 'inline',
                });
                //  save in case of file
                const category = await new InterestModel({
                    name,
                    image: imageUrl,

                    ...this.req.body,
                }).save();
                this.res.status(200).json({
                    status: 200,
                    message: 'success',
                    data: {
                        message: 'Interest added',
                        category,
                    },
                });
            } else {
                //  save in case of no file
                const category = await new InterestModel({
                    name,
                    ...this.req.body,
                }).save();
                this.res.status(200).json({
                    status: 200,
                    message: 'success',
                    data: {
                        message: 'Interest added',
                        category,
                    },
                });
            }
        } catch (error) {
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    category: {},
                },
            });
        }
    };

    index = async () => {
        try {
            // get all interests
            const interest = await InterestModel.find({});
            this.res.status(200).json({
                status: 200,
                message: 'success',
                data: {
                    message: 'Interest list',
                    interest,
                },
            });
        } catch (error) {
            this.res.status(400).json({
                status: 400,
                message: 'error',
                data: {
                    message: error.message,
                    categories: [],
                },
            });
        }
    };
}

export default InterestController;
