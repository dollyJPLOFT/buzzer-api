import { Schema, model, Model } from 'mongoose';
import { IReportProblem } from '../Interface/IReportProblem';

/**
 * schema design for report_problems collection
 */
const reportSchema = new Schema(
    {
        description: { type: String },
    },
    {
        collection: 'report_problems',
        timestamps: true,
    }
);

reportSchema.set('toJSON', {
    transform: (doc, ret, opt) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
    },
});
const ReportProblem: Model<IReportProblem, {}> = model(
    'ReportProblem',
    reportSchema
);
export default ReportProblem;