import { Schema, model, Model } from 'mongoose';
import { IWithdrawal } from '../Interface/IWithdrawal';
/**
 * schema design for withdrawal_requests collection
 */
const withdrawalSchema = new Schema(
    {
        balance: { type: Number, required: true },
        userID: { type: Schema.Types.ObjectId, ref: 'User' },
        accountHolder: { type: String, required: true },
        accountNumber: { type: Number, required: true },
        IFSCcode: { type: String, required: true },
        bankName: { type: String, required: true },
        status: {
            type: String,
            enum: ['Pending', 'Approved', 'Rejected'],
            default: 'Pending'
        },
        updatedBy: { type: Schema.Types.ObjectId, ref: 'User' }
    },
    {
        collection: 'withdrawal_requests',
        timestamps: true,
    }
);

withdrawalSchema.set('toJSON', {
    transform: (doc, ret, opt) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
    },
});
const Withdrawal: Model<IWithdrawal, {}> = model('Withdrawal', withdrawalSchema);
export default Withdrawal;