import { Schema, model, Model } from 'mongoose';
import { IContestVote } from '../Interface/IContestDocument';

/**
 * schema design for contestVote collection
 */
const ContestVoteSchema = new Schema(
    {
        contestId: { type: Schema.Types.ObjectId, ref: 'Contest' },
        voterId: { type: Schema.Types.ObjectId, ref: 'User' },
        videoId: { type: Schema.Types.ObjectId, ref: 'Video' },
    },
    {
        collection: 'contestVote',
        timestamps: true,
    }
);

ContestVoteSchema.set('toJSON', {
    transform: (doc, ret, opt) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
    },
});
const ContestVote: Model<IContestVote, {}> = model(
    'ContestVote',
    ContestVoteSchema
);
export default ContestVote;
