import { Schema, Model, model } from 'mongoose';
import { IContestDocument } from '../Interface/IContestDocument';

/**
 * schema design for contest collection
 */
const properties = {
    timer: { type: Number, default: 5 },
    videos: [
        new Schema(
            {
                videoId: { type: Schema.Types.ObjectId, ref: 'Video' },
                group: { type: Number },
                position: { type: String },
                uploader: { type: Schema.Types.ObjectId, ref: 'User' },
                votingUser: [{ type: Schema.Types.ObjectId, ref: 'User' }],
            },
            { _id: false }
        ),
    ],
    eliminatedUser: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    winningPrize: { type: Number, default: 0 },
    winnerUserId: { type: Schema.Types.ObjectId, ref: 'User' },
    winnerVideoId: { type: Schema.Types.ObjectId, ref: 'Video' },
    winningDate: { type: Date },
    winnerDeclared: { type: Boolean, default: false },
    contestName: { type: String, default: 'Daily' },
    createdBy: { type: String, default: 'automatic' },
    createdById: {
        type: String,
    },
    createdByName: {
        type: String,
    },
    createdByFullName: {
        type: String,
    },
    startDate: { type: Date, default: Date.now },
    endDate: { type: Date, default: Date.now },
    participants: {
        type: Number,
        default: 0
    },
    round1: [], 
    round2: [],
    round3: []
};

const contestSchema = new Schema(properties, {
    collection: 'contests',
    timestamps: true,
});

contestSchema.set('toJSON', {
    transform: (doc, ret, opt) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        delete ret.videos._id;
        return ret;
    },
});

const Contest: Model<IContestDocument, {}> = model('Contest', contestSchema);
export default Contest;
