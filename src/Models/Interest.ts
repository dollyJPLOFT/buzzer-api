import { Schema, model, Model } from 'mongoose';
import { IInterest } from '../Interface/Interest';

/**
 * schema design for interests collection
 */
const InterestSchema = new Schema(
    {
        name: { type: String },
        image: {
            type: String,
            default: `${process.env.HOST}/images/logo.png`,
        },
    },
    {
        collection: 'interests',
        timestamps: true,
    }
);

InterestSchema.set('toJSON', {
    transform: (doc, ret, opt) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
    },
});
const Interest: Model<IInterest, {}> = model('Interest', InterestSchema);
export default Interest;
